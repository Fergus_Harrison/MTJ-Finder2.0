import csv
class MTJSimplifier(object):

    def write_to_file(self, output_list, csvfile):
        if len(output_list) != 0:
            myWriter = csv.writer(csvfile, delimiter= ',', quoting= csv.QUOTE_MINIMAL)
            for element in output_list:
                myWriter.writerow(element)

    def main_method(self, csv_list):
        outfile = open('simplified_' + csvfile.name, 'w', newline="")
        writer = csv.writer(outfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
        writer.writerow(["Structure", "Quaternary structure", "Number of strands in barrel", "number of MTJs", "Stability"])
        self.output_list = []
        self.previous_structure = None
        self.structure = None
        self.quaternary_structure= None
        self.strands_in_barrel = None
        self.MTJ_count = 0
        self.stability = 0
        for entry in csv_list:
            if entry[0] == self.previous_structure:
                print("still on same structure")
                self.MTJ_count += 1
                continue
            else: #is a new structure
                print("starting a new structure")
                self.output_list.append([self.structure,self.quaternary_structure,self.strands_in_barrel,self.MTJ_count,self.stability])
                print(self.output_list)
                self.previous_structure = entry[0]
                print(self.previous_structure)
                self.MTJ_count = 1
                self.structure = entry[0]
                self.quaternary_structure = entry[3]
                self.strands_in_barrel = entry[4]
                self.stability = entry[13]
        self.write_to_file(self.output_list, outfile)

if __name__ == "__main__":
    # with open(input("Enter file name: "), 'r') as csvfile:
    with open("output_file_PDB_FH_revised.csv", 'r') as csvfile:
        reader = csv.reader(csvfile)
        csv_list = list(reader)
    mtj = MTJSimplifier()
    print("about to start main method")
    MTJSimplifier.main_method(mtj, csv_list)

