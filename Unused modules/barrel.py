def barrel_analysis(self, BM, barrel_strands, strand_list, protein_structure, protein_residue_list, protein_id):
        SA = StrandAllocation.StrandAllocation(protein_id, protein_structure, protein_residue_list)
        ps = ProteinStatistics(protein_structure, protein_residue_list)
        axis_points = BM.centroid_determination(barrel_strands) #returns a list with endpoints of the axis
        axis_midpoint = BM.midpoint()
        # BARREL ROTATIONS AND TRANSLATIONS
        #    ROTATE to align with z-axis
        barrel_rotation_matrix = BM.get_barrel_rotation()
        protein_structure = BM.transform(barrel_rotation_matrix, axis_midpoint, protein_structure)
        #    FLIP
        if BM.check_correct_orientation() == False:
            z_flip_matrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, -1]])
            protein_structure = BM.transform(z_flip_matrix, axis_midpoint, protein_structure) #flips the structure along the x-axis, all +z now -z, not really a rotation
        BM = BarrelManipulation.BarrelManipulation(protein_structure, protein_residue_list, strand_list)
        barrel_strands = BM.barrel_determination()
        axis_points = BM.centroid_determination(barrel_strands)  # returns a list with endpoints of the axis

        if len(barrel_strands) >= 2:
            mtj = MTJSearch.MTJSearch(strand_list)
            bs = BarrelStatistics.BarrelStatistics(axis_points, protein_structure, protein_residue_list)
            pi = PDBInfo.PDBInfo(protein_structure, protein_id)
            aromatics_in_protein = mtj.aromatic_list()
            if len(aromatics_in_protein) > 0:
                temp_mtj_df = pd.DataFrame()
                for aromatic in aromatics_in_protein:
                    neighbouring_gly = mtj.check_for_gly(aromatic)
                    print(neighbouring_gly)
                    if neighbouring_gly != False: #aromatic neighbours a glycine
                        for glycine in neighbouring_gly:
                            #TODO check that the expanded glycine radius search produces false MTJs that look correct (pymol task).
                            #get all data
                            in_out_arom = bs.inner_outer_determination(aromatic)
                            in_out_gly = bs.inner_outer_determination(glycine)
                            if in_out_arom != in_out_gly:
                                continue
                            else:
                                print(aromatic)
                                temporary_df = pd.DataFrame(index=[0])
                                arom_residue_name = aromatic.get_resname()
                                arom_residue_num = aromatic.get_full_id()[3][1]
                                gly_residue_num = glycine.get_full_id()[3][1]
                                aromatic_phi, aromatic_psi = SA.get_phipsi(aromatic)
                                glycine_phi, glycine_psi = SA.get_phipsi(glycine)
                                AN_residue, AC_residue = ps.res_adj_AAs(aromatic)
                                GN_residue, GC_residue = ps.res_adj_AAs(glycine)
                                protein_weight = ps.protein_weight()
                                rear_residue = None
                                structure_resolution = pi.protein_resolution()
                                protein_name = pi.protein_name()
                                protein_family = pi.protein_family()
                                quaternary = pi.quaternary_structure()
                                elucidation_method = pi.elucidation_method()
                                num_strands_in_barrel = len(barrel_strands)
                                vertical_placement = bs.vertical_placement(glycine)
                                rear_residue_list = bs.search_for_rear_residue(aromatic)
                                for residue in rear_residue_list:
                                    if residue != None:
                                        if bs.inner_outer_determination(residue) != in_out_arom:
                                            continue
                                        else:
                                            rear_residue = residue
                                if rear_residue != None:
                                    rear_residue_name = rear_residue.get_resname()
                                    rear_residue_num = rear_residue.get_full_id()[3][1]
                                    RN_residue, RC_residue = ps.res_adj_AAs(rear_residue)
                                else:
                                    rear_residue_name = "None"
                                    rear_residue_num = "None"
                                    RN_residue, RC_residue = "None", "None"
                                contains_barrel = True
                                correct_torsion, chi1, chi2 = mtj.check_torsion(aromatic)
                                if correct_torsion == "BROKEN":
                                    continue
                                rca = ClashAnalysis(protein_structure, aromatic, chi1)
                                clash_analysis_df = rca.main()
                                temporary_df['Protein_id'] = protein_id
                                temporary_df['elucidation_method'] = elucidation_method
                                temporary_df['Resolution'] = structure_resolution
                                temporary_df['species'] = self.species
                                temporary_df['protein_weight'] = protein_weight
                                temporary_df['protein_name'] = self.protein_name
                                temporary_df['protein_family'] = protein_family
                                temporary_df['quaternary'] = quaternary
                                temporary_df['contains_barrel'] = contains_barrel
                                temporary_df['num_strands_in_barrel'] = num_strands_in_barrel
                                temporary_df['arom_residue_name'] = arom_residue_name
                                temporary_df['arom_residue_num'] = arom_residue_num
                                temporary_df['gly_residue_num'] = gly_residue_num
                                temporary_df['aromatic_phi'] = aromatic_phi
                                temporary_df['aromatic_psi'] = aromatic_psi
                                temporary_df['glycine_phi'] = glycine_phi
                                temporary_df['glycine_psi'] = glycine_psi
                                temporary_df['chi1'] = chi1
                                temporary_df['chi2'] = chi2
                                temporary_df['native_chi1_rotamer'] = rca.current_rotamer(chi1)
                                temporary_df['vertical_placement'] = vertical_placement
                                if (aromatic['O']-glycine['N']) < 3.5: #tries to determine if the residues are hydrogen bonded by measuring the distance between the oxygen and nitrogen of the two residues
                                    temporary_df['hydrogen_bonded'] = 1
                                else:
                                    temporary_df['hydrogen_bonded'] = 0
                                temporary_df['AN_residue'] = AN_residue
                                temporary_df['AC_residue'] = AC_residue
                                temporary_df['GN_residue'] = GN_residue
                                temporary_df['GC_residue'] = GC_residue
                                temporary_df['rear_residue_name'] = rear_residue_name
                                temporary_df['rear_residue_num'] = rear_residue_num
                                temporary_df['RN_residue'] = RN_residue
                                temporary_df['RC_residue'] = RC_residue
                                temporary_df = pd.concat([temporary_df, clash_analysis_df], axis=1, join='outer')
                                A_G_dist = mtj.distance_measure(aromatic, glycine)
                                temporary_df['Distance b/w residues'] = A_G_dist                      
                                if A_G_dist <= 5 and 80<abs(chi2)<100:
                                    # aromatic is true mtj
                                    print('accepted as true')
                                    temporary_df['true/false'] = 1
                                    temp_mtj_df =temp_mtj_df.append(temporary_df, sort=False)                                   
                                else:
                                    #is a false mtj
                                    print("rejected as false")
                                    temporary_df['true/false'] = 0
                                    temp_mtj_df =temp_mtj_df.append(temporary_df, sort=False)
                return temp_mtj_df
  