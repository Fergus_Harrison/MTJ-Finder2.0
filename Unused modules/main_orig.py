import csv
import logging
import os

import numpy as np
import pandas as pd
from Bio.PDB import PDBIO, PDBList, PDBParser, Selection

import BarrelManipulation
import BarrelStatistics
import MTJSearch
import PDBInfo
import StrandAllocation
from RotamerClashAnalysis import ClashAnalysis

logging.getLogger('matplotlib').setLevel(logging.WARNING) 

class MTJFinder(object):

    def fetch_structure(self, structure_id):
        """ Takes structure ID to download the corresponding pdb file."""
        PDBList().retrieve_pdb_file(structure_id, False, "PDB", 'pdb') #downloads the file to directed subfolder "PDB"
        directory = os.path.dirname(__file__)
        dirname = os.path.join(directory, 'PDB', "pdb" + structure_id + ".ent")
        structure = PDBParser().get_structure(structure_id, dirname) #Parses already downloaded file to be used
        return structure

    def fetch_residue_list(self, structure):
        """ Takes structure and returns the first chain of the structure as a list of residues. """
        self.model_list = Selection.unfold_entities(structure, 'M') # M for model, determines if multiple models exist
        self.chain_list = Selection.unfold_entities(structure, 'C')  # C for chains
        residue_list = Selection.unfold_entities(self.chain_list[0], 'R')  # R for residues
        return residue_list  # returns the PDB as a list of individual residues and their metadata

    def chain_only_structure(self,structure):
        models = Selection.unfold_entities(structure, 'M')
        chains = Selection.unfold_entities(structure, 'C')
        new_structure = chains[0]
        return new_structure

    def save_structure(self, structure, structure_id):
         io = PDBIO()
         io.set_structure(structure)
         directory = os.path.dirname(__file__)
         pdb_name = os.path.join(directory, 'PDB', "pdb" + structure_id + ".ent")
         io.save(pdb_name)
         return

    def write_to_file(self, output_list, csvfile):
        ## Takes any list, or nested-list and writes each list to a new line.
        if len(output_list) != 0:
            myWriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
            for element in output_list:
                myWriter.writerow(element)

    def name_cleaner(self, item):
        # removes the ".pdb" tag found on the structure name if present
        cleaned_item = item[0]
        if cleaned_item.endswith('.pdb'):
            cleaned_item = cleaned_item[:-4]
        if cleaned_item.startswith(' '):
            cleaned_item = cleaned_item[1:]
        return cleaned_item.lower()

    def iterate_list(self, protein_list):

        false_mtj_df = pd.DataFrame()
        true_mtj_df = pd.DataFrame()

        self.amount_of_structures_with_MTJ = 0
        screened_proteins = []
        for protein in protein_list:
            print("")
            protein_id = self.name_cleaner(protein)
            protein_structure = self.fetch_structure(protein_id)
            pi = PDBInfo.PDBInfo(protein_structure, protein_id)
            protein_name = pi.protein_name()
            species = pi.species_origin()
            print(protein_name)
            if protein_name != None and species != None:
                # records the species protein combination by concatenating the two together and saving it in a list, just as effective as a more complicated datastructure
                if species+protein_name in screened_proteins:                     
                    print("Structure already screened")
                    continue
                else:
                    screened_proteins.append(species+protein_name)
            protein_structure = self.chain_only_structure(protein_structure)
            protein_residue_list = self.fetch_residue_list(protein_structure)
            SA = StrandAllocation.StrandAllocation(protein_id, protein_structure, protein_residue_list)
            strand_list = SA.strand_calculator()
            if len(strand_list) < 2:
                continue
            BM = BarrelManipulation.BarrelManipulation(protein_structure, protein_residue_list, strand_list)
            barrel_strands = BM.barrel_determination()
            print(len(barrel_strands))
            axis_points = BM.centroid_determination(barrel_strands) #returns a list with endpoints of the axis
            axis_midpoint = BM.midpoint()
            # BARREL ROTATIONS AND TRANSLATIONS
            #    ROTATE to align with z-axis
            barrel_rotation_matrix = BM.get_barrel_rotation()
            protein_structure = BM.transform(barrel_rotation_matrix, axis_midpoint, protein_structure)
            #    FLIP
            if BM.check_correct_orientation() == False:
                z_flip_matrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, -1]])
                protein_structure = BM.transform(z_flip_matrix, axis_midpoint, protein_structure) #flips the structure along the x-axis, all +z now -z, not really a rotation
            BM = BarrelManipulation.BarrelManipulation(protein_structure, protein_residue_list, strand_list)
            barrel_strands = BM.barrel_determination()
            axis_points = BM.centroid_determination(barrel_strands)  # returns a list with endpoints of the axis

            if len(barrel_strands) >= 2:
                mtj = MTJSearch.MTJSearch(strand_list)
                bs = BarrelStatistics.BarrelStatistics(axis_points, protein_structure, protein_residue_list)
                pi = PDBInfo.PDBInfo(protein_structure, protein_id)
                aromatics_in_protein = mtj.aromatic_list()
                if len(aromatics_in_protein) > 0:
                    for aromatic in aromatics_in_protein:
                        neighbouring_gly = mtj.check_for_gly(aromatic)
                        if neighbouring_gly != False: #aromatic neighbours a glycine
                            for glycine in neighbouring_gly:
                                #get all data
                                in_out_arom = bs.inner_outer_determination(aromatic)
                                in_out_gly = bs.inner_outer_determination(glycine)
                                if in_out_arom != in_out_gly:
                                    continue
                                else:
                                    print(aromatic)
                                    temporary_df = pd.DataFrame(index=[0])
                                    num_strands_in_barrel = len(barrel_strands)
                                    arom_residue_name = aromatic.get_resname()
                                    arom_residue_num = aromatic.get_full_id()[3][1]
                                    gly_residue_num = glycine.get_full_id()[3][1]
                                    aromatic_phi, aromatic_psi = SA.get_phipsi(aromatic)
                                    glycine_phi, glycine_psi = SA.get_phipsi(glycine)
                                    vertical_placement = bs.vertical_placement(glycine)
                                    AN_residue, AC_residue = bs.res_adj_AAs(aromatic)
                                    GN_residue, GC_residue = bs.res_adj_AAs(glycine)
                                    protein_weight = bs.protein_weight()
                                    rear_residue = None
                                    structure_resolution = pi.protein_resolution()
                                    protein_name = pi.protein_name()
                                    protein_family = pi.protein_family()
                                    quaternary = pi.quaternary_structure()
                                    elucidation_method = pi.elucidation_method()
                                    rear_residue_list = bs.search_for_rear_residue(aromatic)
                                    for residue in rear_residue_list:
                                        if residue != None:
                                            if bs.inner_outer_determination(residue) != in_out_arom:
                                                continue
                                            else:
                                                rear_residue = residue
                                    if rear_residue != None:
                                        rear_residue_name = rear_residue.get_resname()
                                        rear_residue_num = rear_residue.get_full_id()[3][1]
                                        RN_residue, RC_residue = bs.res_adj_AAs(rear_residue)
                                    else:
                                        rear_residue_name = "None"
                                        rear_residue_num = "None"
                                        RN_residue, RC_residue = "None", "None"
                                    if len(barrel_strands) > 0:
                                        contains_barrel = True
                                    else:
                                        contains_barrel = False
                                    correct_torsion, chi1, chi2 = mtj.check_torsion(aromatic)
                                    if correct_torsion == "BROKEN":
                                        continue
                                    rca = ClashAnalysis(protein_structure, aromatic, chi1)
                                    clash_analysis_df = rca.main()
                                    temporary_df['Protein_id'] = protein_id
                                    temporary_df['elucidation_method'] = elucidation_method
                                    temporary_df['Resolution'] = structure_resolution
                                    temporary_df['species'] = species
                                    temporary_df['protein_weight'] = protein_weight
                                    temporary_df['protein_name'] = protein_name
                                    temporary_df['protein_family'] = protein_family
                                    temporary_df['quaternary'] = quaternary
                                    temporary_df['contains_barrel'] = contains_barrel
                                    temporary_df['num_strands_in_barrel'] = num_strands_in_barrel
                                    temporary_df['arom_residue_name'] = arom_residue_name
                                    temporary_df['arom_residue_num'] = arom_residue_num
                                    temporary_df['gly_residue_num'] = gly_residue_num
                                    temporary_df['aromatic_phi'] = aromatic_phi
                                    temporary_df['aromatic_psi'] = aromatic_psi
                                    temporary_df['glycine_phi'] = glycine_phi
                                    temporary_df['glycine_psi'] = glycine_psi
                                    temporary_df['chi1'] = chi1
                                    temporary_df['chi2'] = chi2
                                    temporary_df['native_chi1_rotamer'] = rca.current_rotamer(chi1)
                                    temporary_df['vertical_placement'] = vertical_placement
                                    temporary_df['AN_residue'] = AN_residue
                                    temporary_df['AC_residue'] = AC_residue
                                    temporary_df['GN_residue'] = GN_residue
                                    temporary_df['GC_residue'] = GC_residue
                                    temporary_df['rear_residue_name'] = rear_residue_name
                                    temporary_df['rear_residue_num'] = rear_residue_num
                                    temporary_df['RN_residue'] = RN_residue
                                    temporary_df['RC_residue'] = RC_residue
                                    temporary_df = pd.concat([temporary_df, clash_analysis_df], axis=1, join='outer')

                                    A_G_dist = mtj.distance_measure(aromatic, glycine)
                                    temporary_df['Distance b/w residues'] = A_G_dist
                                    
                                    if correct_torsion == False:
                                        # aromatic is false mtj
                                        print('rejected as false')
                                        false_mtj_df = false_mtj_df.append(temporary_df, sort = False)
                
                                    else:
                                        print("accepted as true")
                                        true_mtj_df = true_mtj_df.append(temporary_df, sort = False)
                                        
        print("FINISHED STRUCTURE(S)")

        self.amount_of_structures_in_list = len(protein_list)
        true_columns = list(true_mtj_df.columns.values)
        false_columns = list(false_mtj_df.columns.values)
        true_mtj_df.to_csv('output_file_true {}'.format(csvfile.name), columns=true_columns)
        false_mtj_df.to_csv('output_file_false {}'.format(csvfile.name), columns=false_columns)
        print(true_mtj_df)  
        print(false_mtj_df)      


if __name__ == "__main__":
    with open(input("Enter file name: "), 'r') as csvfile:
    # with open("PDB_FH_revised.csv", 'r') as csvfile:
    # with open("first.csv", 'r') as csvfile:
        reader = csv.reader(csvfile)
        csv_list = list(reader)
    mtj = MTJFinder()
    MTJFinder.iterate_list(mtj, csv_list)











'''
        # ALTERNATE (dunbrack method): generate hydrogens for all nitrogen atoms in the backbone and determine if in strand by a mixture of phi/psi compliance
        # AND hydrogen bonding to amino acid with beta-strand assigned AA (Could be more computationally intensive, will test both approaches)

        # Beta-barrel determination:
        #DONE Ellipse defined by best fit of the top of each strand in the barrel.
        #DONE Define centre axis of the barrel by finding the centre points of ellipses at the top and bottom of the barrel and drawing a vector between them).
        # Offset the barrel along the z-axis so that the centroid of the bottom ellipse is at -12A (I believe this is to gain an approximation of its true position in the membrane) (slusky et al)
        #DONE Rotate barrel axis to z axis. with the internal side pointing to the -ve end of the number line.
        #DONE Determine orientation of the barrel from the rule: both N and C terminal of the barrel are internal (Schulz, 2002)
        # MTJ search prerequisite setup:
        # DONE if protein contains beta strands (check list of B-stands for the protein)
        # DONE Create list of aromatic residues
        # MTJ search
        # Search list of aromatic residues for residues that are adjacent to a glycine, regardless of aromatic chi angle (use Biopython neighbourSearch, might be a better search with the generated hydrogens).
        # If aromatic contains correct chi angles (45<chi1<75, 80<chi2<100) then it's considered a true MTJ.
        # If aromatic does not contain correct chi angles, then it's considered a false MTJ
        # All recordings of both true and false MTJs will contain: true or false MTJ, aromatic name, aromatic num, glycine num, distance b/w aromatic and glycine, Chi1/chi2.
        # Check relative location of the MTJ to the centre of the protein barrel axis (as a function of the z-axis only).
        # Positive number = it is closer to the outside, negative number = closer to the periplasm.
        # for both true and false MTJs find the inward facing residues to the left and right of the glycine, aromatic and left and right of the residue behind the aromatic.
        # Left and right residues can be determined using protein sequence (+-2 from residue of interest)
        # Rear residue can be determined using proximity of residue to the aromatic on a neighbouring strand (and is facing the same direction as the aromatic in/out).
        # To determine if the residue is facing inwards (dunbrack): compare the angle between the z-axis and the midpoint of its amino nitrogen and carbonyl carbon to its carbon alpha
        # If angle is <90 then it is facing inwards.
        # Basic INFO of protein (only IF MTJ has been found in protein):
        # PDB Header reader
        # Check protein name
        # If not, refer to Scope database
        # Check protein resolution
        # Check elucidation method
        # Check species of original organism protein is found in.
        # Check quaternary structure
        # If not, refer to Scope Database
        # Scope database reader/referencer
        # Check protein family
        # Calculate protein size in KDa
        # Either call a library or write own, based off size of each amino acid.

# FINAL output values
#    DONE structure code
#    DONE structure resolution
#    DONE elucidation method
#    DONE protein name
#    DONE protein family
#    DONE Species of organism
#    DONE quaternary structure
#    DONE protein size
#    DONE Boolean: Contains barrel
#    DONE number of strands in barrel
#    DONE Is the MTJ inner or outer (reference arom_in_out variable)
#    DONE aromatic residue adjacent to the glycine
#    DONE Residue number of the aromatic
#    DONE residue number of the glycine
#    DONE Distance between specific atoms in the aromatic residue and the glycine residue
#    DONE Chi1
#    DONE Chi2
#    DONE gly and arom Phi
#    DONE gly and arom Psi
#    DONE YNResidue
#    DONE YCResidue
#    DONE GNResidue
#    DONE GCResidue
#    DONE Rear Residue
#    DONE RearN Residue
#    DONE RearC Residue
#    DONE distance of joint from vertical centre of barrel

'''
