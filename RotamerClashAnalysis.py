# This module is designed to take a structure, and a aromatic residue. The module will then detemine 
# the neighbouring atoms of each atom in the residue, then calculate the interaction energy between 
# the residue atom and Non-bonded neighbouring atoms. It will do this for each atom in the residue 
# and summarise the rotamers clash score as a summarisation of the clashes and as an average clash/atom.
# This process will then repeat for each rotamer available to the residue (normally 3 rotamers in total)
# Heavily inspired by Ramachandran, S., Kota, P., Ding, F., & Dokholyan, N. V. (2010). Automated minimization of steric clashes in protein structures. Proteins: Structure, Function, and Bioinformatics, 79(1), 261–270. doi:10.1002/prot.22879 

import logging
import os
from collections import OrderedDict
from pathlib import Path

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from Bio.PDB import PDBIO, NeighborSearch, Selection, vectors
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

import get_torsion
import ProteinStatistics as ps
from CHARMMparser import CHARMMparser

logging.getLogger('matplotlib').setLevel(logging.WARNING) 
logging.getLogger('Bio.PDB').setLevel(logging.WARNING) 
class ClashAnalysis():
    def __init__(self, structure, residue, chi1):
        self.structure = structure
        
        self.target_residue = residue
        self.chi1 = chi1
        self.accepted_atoms = ['C', 'CA', 'CB', 'CD', 'CD1', 'CD2', 'CE', 'CE1', 'CE2', 'CE3', 'CG', 'CG1', 
                               'CG2', 'CH2', 'CL', 'CLP', 'CR', 'CRP', 'CZ', 'CZ2','CZ3', 'N', 'ND1', 'ND2', 'NE', 'NE1', 'NE2', 'NH1', 'NH2', 
                               'NL', 'NR', 'NZ', 'O', 'OD1', 'OD2', 'OE1', 'OE2', 'OG', 'OG1', 'OH', 'OL', 'OR', 'OXT',
                               'SD', 'SG'] 
        self.hydrogen_atoms =  ['H', 'HA', 'HA1', 'HA2', 'HA3',
                               'HB', 'HB1', 'HB2', 'HB3', 'HD1', 'HD11', 'HD12', 'HD13', 'HD2', 'HD21', 'HD22', 
                               'HD23', 'HD3', 'HE', 'HE1', 'HE2', 'HE21', 'HE22', 'HE3', 'HG', 'HG1', 'HG11', 
                               'HG12', 'HG13', 'HG2', 'HG21', 'HG22', 'HG23', 'HH', 'HH11', 'HH12', 'HH2', 
                               'HH21', 'HH22', 'HL', 'HL1', 'HL2', 'HL3', 'HN', 'HR', 'HR1', 'HR2', 'HR3', 
                               'HZ', 'HZ1', 'HZ2', 'HZ3']
        self.accepted_residues = ['ALA', 'ALAD', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY', 'HSD', 
                                  'HSE', 'HSP', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 
                                  'TRP', 'TYR', 'VAL']
        self.structure_residues = list(structure.get_residues())
        # print("last residue:", self.structure_residues[-1])

    def main(self):
        '''method responsible for coordinating the rotation of a
        residue by ~120 degrees (changes depending on residue) and measuring the steric interaction observed in each rotamer.'''
        # figure out the current rotamer and the other two likely rotamers, take ideal rotamer positions from dunbrack
        self.saved_residue = self.target_residue.copy()
        rotamers_energies = pd.DataFrame(index=[0])
        rotamers = self.rotamer_list(self.target_residue)
        current_rot_name = self.current_rotamer(self.chi1)
        new_rot_order = self.change_dict_order(rotamers, current_rot_name)
        new_rot_order[current_rot_name] = self.chi1
        for key in new_rot_order:
            # print(key)
            non_rotate_list = ['N', 'CA', 'C', 'O', 'CB'] #true list
            terminal_non_rotate_list = ['N', 'CA', 'C', 'O', 'CB', 'OXT'] #true list
            current_chi1 = get_torsion.calculate_torsion(chi=[1], res=self.target_residue)[0]
            needed_chi1 = new_rot_order[key]
            degrees_to_rotate = needed_chi1 - current_chi1
            if degrees_to_rotate < -180:
                degrees_to_rotate += 360
            # print('current:', current_chi1)
            # print('needed:', needed_chi1)
            # print("DEG:",degrees_to_rotate)
            rads_to_rotate = np.radians(degrees_to_rotate)
            old_target_residue = self.target_residue.copy()
            #+rads_to_rotate for anti_clockwise rotation
            self.rotate_residue(self.target_residue, rads_to_rotate, non_rotate_list)
            # print("new chi", get_torsion.calculate_torsion(chi=[1], res=self.target_residue)[0],'\n')
            # self.save_structure(self.structure, self.structure.get_id()+key+str(self.target_residue.get_full_id()[3][1]))
            # self.visualise([self.target_residue, old_target_residue], 'c')
            energy_total = 0
            for atom in self.target_residue:
                if atom.get_name() in self.hydrogen_atoms or atom.get_name().startswith('H'):
                    # print('This structure contains hydrogens, ignoring hydrogens')
                    continue
                interaction_total = 0
                if atom.get_name() in non_rotate_list or atom in terminal_non_rotate_list:
                    # print("skipping atom", atom.get_name())
                    continue                    
                else:
                    surrounding_atoms = self.neighbour_search(atom)
                    neighbour_count=0
                    for neighbour in surrounding_atoms:
                        if neighbour.get_parent().get_resname().strip() not in self.accepted_residues:
                            if neighbour.get_parent().get_resname().strip() == "HIS":
                                neighbour.get_parent().resname = "HSE"
                            else:
                                print("This atom is either not an accepted atom for CHARMM or is part of a residue that is not supported by CHARMM")
                                print('residue:',neighbour.get_parent().get_resname().strip())
                                print('atom:', neighbour.get_name())
                                raise
                        elif neighbour.get_name() not in self.accepted_atoms:
                            # if neighbour.get_name() == "OXT":
                            #     #TODO FIGURE OUT A WAY TO ACCOUNT FOR TERMINAL VALUES INSTEAD OF TREATING LIKE NON-Terminal
                            #     neighbour.name = "O"
                            #     neighbour.fullname = " O "
                            #     neighbour.id = "O"
                            if neighbour.get_name() in self.hydrogen_atoms or neighbour.get_name().startswith('H'):
                                # print('This structure contains hydrogens, ignoring hydrogens')
                                continue
                            else:
                                print("This atom is either not an accepted atom for CHARMM or is part of a residue that is not supported by CHARMM")
                                print('residue:',neighbour.get_parent().get_resname().strip())
                                print('atom:', neighbour.get_name())
                                continue #ignores this neighbour and moves on. May affect calcs for structures with odd naming of atoms                      
                        neighbour_count+=1
                        interaction_total += self.calc_interaction_energy(atom, neighbour)
                    try:
                        # print(interaction_total, neighbour_count, atom.get_name())
                        interaction_score = interaction_total/neighbour_count
                    except ZeroDivisionError:
                        interaction_score = 0
                    energy_total+=interaction_score
            if self.target_residue.get_segid() == self.structure_residues[-1].get_segid():
                energy_score = energy_total/(len(self.target_residue) - len(terminal_non_rotate_list))
            else:
                energy_score = energy_total/(len(self.target_residue) - len(non_rotate_list))
            column_name_total = key+'total'
            column_name_score = key+'score'
            rotamers_energies.at[0, column_name_total] = energy_total
            rotamers_energies.at[0, column_name_score] = energy_score
        for atom in self.target_residue: # translate all the atoms back to where they came from
            new_coord = self.saved_residue[atom.get_id()].get_coord()
            atom.set_coord(new_coord)
        rotamers_energies['lowest_rotamer_energy'] = self.lowest_rotamer_energy(rotamers_energies)
        return rotamers_energies

    def lowest_rotamer_energy(self, dataframe):
        '''Determines which rotamer was measured to have 
        the lowest interaction energy with its surroundings.'''
        lowest_score_rotamer = dataframe[['+gauchescore', 'transscore', '-gauchescore']].idxmin(axis=1).str.replace('score','')
        return lowest_score_rotamer #returns a series

    def visualise(self, residues, col):
        '''method used to check residue rotation is being performed correctly'''
        Xs=[]
        Ys=[]
        Zs=[]
        Xs1=[]
        Ys1=[]
        Zs1=[]
        for atom in residues[0]:
            Xs.append(atom.get_coord()[0])
            Ys.append(atom.get_coord()[1])
            Zs.append(atom.get_coord()[2])
        for atom in residues[1]:
            Xs1.append(atom.get_coord()[0])
            Ys1.append(atom.get_coord()[1])
            Zs1.append(atom.get_coord()[2])
        # if 'fig' not in locals():
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(Xs, Ys, Zs, c='c')
        ax.scatter(Xs1, Ys1, Zs1, c='r')
        plt.show()
        return

    def change_dict_order(self, dic, first):
        '''changes the order of an orderDict so that the native rotamer is the first rotamer measured.
        This is so the rotation of the rotamer is almost garunteed to be correct and saves computation'''
        new_dict = OrderedDict()
        new_dict[first] = dic.pop(first)
        new_dict.update(dic)
        return new_dict

    def rotamer_list(self, residue):
        '''A method to figure out current rotation bin and the angles of the other 2 rotamers
           returns dict of rotamers and the associated angles. Rotamer angles are taken from http://www.dynameomics.org/rotamer/
           where the most common chi1chi2 occurence was assumed to also be the most common chi1 occurence for each rotamer of each
           residue. Further work could be done on calculating the ideal rotamer but for prrof of concept this is acceptable.'''
        # res_common = {'TYR_common':{'+gauche': 51.7,
        #                             'trans':182.5,
        #                             '-gauche':295.3},
        #                 'TRP_common':{'+gauche': 53.6,
        #                             'trans':181.8,
        #                             '-gauche':294.7},
        #                 'PHE_common':{'+gauche': 52.0,
        #                             'trans':182.8,
        #                             '-gauche':294.3}}
        res_common = {'TYR_common':{'+gauche': 51.7,
                                    'trans':182.5-360,
                                    '-gauche':295.3-360},
                        'TRP_common':{'+gauche': 53.6,
                                    'trans':181.8-360,
                                    '-gauche':294.7-360},
                        'PHE_common':{'+gauche': 52.0,
                                    'trans':182.8-360,
                                    '-gauche':294.3-360}}
        dict_name = residue.get_resname()+"_common"
        return res_common[dict_name]
 
    def current_rotamer(self, chi1):
        '''Figures out which rotamer that the native rotamer belongs to'''
        #TODO should probably look at adding chi2 angle consideration for the rotamer choices
        rot_defs = {'+gauche':(0,120),
                    'trans':(120,240),
                    '-gauche':(240,360)} 
        curr_rot = None
        if chi1 < 0:
            chi1+=360
        for entry in rot_defs:
            if rot_defs[entry][0] < chi1 <= rot_defs[entry][1]:
                curr_rot = entry
        return curr_rot #returns a string

    def rotate_residue(self, residue, radians, skip=[]):
        ''' takes a residue and rotates the atoms around the 
            stem of the residue. edits the accessible structure.'''
        trans_atom = residue['CA'].get_vector() # centre atom to transform the whole residue to the origin
        for atom in residue: # Translate all the atoms down around the origin
            new_coord = atom.get_vector()-trans_atom
            atom.set_coord(new_coord)
        CA = residue['CA'].get_vector()
        CB = residue['CB'].get_vector()
        rotation_axis = CB-CA
        # rotation_matrix = vectors.rotaxis(radians, rotation_axis)
        rot_matrix = self.rotation_matrix(radians, rotation_axis)

        for atom in residue: # rotate all the atoms around the axis
            if atom.get_id() in skip:
                continue
            else:
                new_coord = atom.get_vector().left_multiply(rot_matrix) 
                # new_coord = atom.get_vector().right_multiply(rot_matrix)
                atom.set_coord(new_coord.get_array())
        for atom in residue: # translate all the atoms back to where they came from
            new_coord = atom.get_vector()+trans_atom
            atom.set_coord(new_coord.get_array())
        #Check rotamer is correct



    def neighbour_search(self, atom):
        '''method that searches a sphere around an atom for 
        nonbonded atoms, returns a list of those atoms.'''
        search_distance = 4.5
        atom_list = Selection.unfold_entities(self.structure, 'A')  # Breaks each residue in the list into their corresponding atoms and coordinates
        ns = NeighborSearch(atom_list)  # initialises NeighborSearch function
        centre = atom.get_coord()
        found_in_radius = ns.search(centre, search_distance, level='A')
        neighbour_list = []
        for atm in found_in_radius:
            residue_in_radius = atm.get_parent()
            if residue_in_radius.get_full_id()[3][1] != self.target_residue.get_full_id()[3][1]: #Checks that atom isn't part of same residue 
                neighbour_list.append(atm)
        return neighbour_list
        
    def calc_interaction_energy(self, atom1, atom2):
        '''receives both atoms to calculate on and uses the CHARMM forcefield calculations.'''
        ch = CHARMMparser()
        atom1_name = atom1.get_name()
        atom2_name = atom2.get_name()
        residue1 = atom1.get_parent().get_resname()
        residue2 = atom2.get_parent().get_resname()
        distance = atom1-atom2
        clash_energy = ch.unbonded_interaction(residue1, atom1_name, residue2, atom2_name, distance)
        # if clash_energy>100:
        #     print(atom1.get_parent(), atom2.get_parent())
        #     print('clash energy:', clash_energy)
        return clash_energy 

    def save_structure(self, structure, structure_id):
         '''saves structure as a .ent file into the main directory'''
         io = PDBIO()
         io.set_structure(structure)
         directory = os.path.dirname(__file__)
        #  pdb_name = os.path.join(directory, 'test_pdb', "pdb" + structure_id + ".ent")
         pdb_name = Path('./test_pdb/pdb{}.ent'.format(structure_id))
         io.save(pdb_name)
         return

    def rotation_matrix(self, angle, direction, point=None):
        """ Return matrix to rotate about axis defined by point and direction.
        R = rotation_matrix(math.pi/2, [0, 0, 1], [1, 0, 0])
        numpy.allclose(numpy.dot(R, [0, 0, 0, 1]), [1, -1, 0, 1])
        True
        angle = (random.random() - 0.5) * (2*math.pi)
        direc = numpy.random.random(3) - 0.5
        point = numpy.random.random(3) - 0.5
        R0 = rotation_matrix(angle, direc, point)
        R1 = rotation_matrix(angle-2*math.pi, direc, point)
        is_same_transform(R0, R1)
        True
        R0 = rotation_matrix(angle, direc, point)
        R1 = rotation_matrix(-angle, -direc, point)
        is_same_transform(R0, R1)
        True
        I = numpy.identity(4, numpy.float64)
        numpy.allclose(I, rotation_matrix(math.pi*2, direc))
        True
        numpy.allclose(2, numpy.trace(rotation_matrix(math.pi/2,
                                                    direc, point)))
        True

        """
        import math
        sina = math.sin(angle)
        cosa = math.cos(angle)
        # direction = unit_vector(direction[:3])
        direction = ps.normalise_vector(direction[:3])
        # rotation matrix around unit vector
        R = np.diag([cosa, cosa, cosa])
        R += np.outer(direction, direction) * (1.0 - cosa)
        direction *= sina
        R += np.array([[  0.0,          -direction[2], direction[1]],
                        [ direction[2], 0.0,           -direction[0]],
                        [-direction[1], direction[0],  0.0]])
        return R
        # M = np.identity(4)
        # M[:3, :3] = R
        # if point is not None:
        #     # rotation not around origin
        #     point = np.array(point[:3], dtype=np.float64, copy=False)
        #     M[:3, 3] = point - np.dot(R, point)
        # print(np.array(M))
        # return M
