import csv
import logging
import os
import warnings

import numpy as np
import pandas as pd
from Bio import BiopythonWarning
from Bio.PDB import PDBIO, PDBList, PDBParser, Selection, Vector

import BarrelManipulation
import BarrelStatistics
import MTJSearch
import PDBInfo
import StrandAllocation
from ProteinStatistics import ProteinStatistics
from RotamerClashAnalysis import ClashAnalysis
from PDB_processing import PDB_prep
from pathlib import Path

# warnings.simplefilter('ignore', BiopythonWarning)
logging.getLogger('matplotlib').setLevel(logging.WARNING) 

class MTJFinder(object):
    def __init__(self):
        self.accepted_residues = ['ALA', 'ALAD', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY','HIS', 'HSD', 
                                  'HSE', 'HSP', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 
                                  'TRP', 'TYR', 'VAL']

    def fetch_residue_list(self, structure):
        """ Takes structure and returns the first chain of the structure as a list of residues. """
        self.model_list = Selection.unfold_entities(structure, 'M') # M for model, determines if multiple models exist
        self.chain_list = Selection.unfold_entities(structure, 'C')  # C for chains
        residue_list = Selection.unfold_entities(self.chain_list[0], 'R')  # R for residues
        return residue_list  # returns the PDB as a list of individual residues and their metadata

   
    def write_to_file(self, output_list, csvfile):
        '''Takes any list, or nested-list and writes each list to a new line.
        Made redundant because of pandas modules'''
        if len(output_list) != 0:
            myWriter = csv.writer(csvfile, delimiter=',', quoting=csv.QUOTE_MINIMAL)
            for element in output_list:
                myWriter.writerow(element)

    def name_cleaner(self, item):
        '''returns a cleaned pdbfile id after it removes the ".pdb" tag 
        found on the structure name if present. Also strips whitespaces'''
        cleaned_item = item[0].strip()
        if cleaned_item.endswith('.pdb'):
            cleaned_item = cleaned_item[:-4]
        return cleaned_item.lower()

    def iterate_list(self, protein_list, filename):
        '''Iterates over list of pdb file ids and calculates all the information for that structure.
        It then saves all the data to a dataframe which is then saved at the end of the program'''
        mtj_df = pd.DataFrame()
        pdbp = PDB_prep()
        screened_proteins = []
        for protein in protein_list:
            print("")
            protein_id = self.name_cleaner(protein)
            protein_structure = pdbp.fetch_structure(protein_id)
            pi = PDBInfo.PDBInfo(protein_structure, protein_id)
            self.protein_name = pi.protein_name()
            self.species = pi.species_origin()
            print(self.protein_name)
            if self.protein_name != None and self.species != None:
                # records the species protein combination by concatenating the two together and saving it in a list
                if self.species + self.protein_name in screened_proteins:                     
                    print("Structure already screened")
                    continue
                else:
                    screened_proteins.append(self.species + self.protein_name)
            protein_structure = pdbp.chain_only_structure(protein_structure)
            protein_residue_list = self.fetch_residue_list(protein_structure)
            SA = StrandAllocation.StrandAllocation(protein_id, protein_structure, protein_residue_list)
            strand_list = SA.strand_calculator()
            
            if len(strand_list) < 2:
                continue
            BM = BarrelManipulation.BarrelManipulation(protein_structure, protein_residue_list, strand_list)
            barrel_strands = BM.barrel_determination()
            print('# of strands in barrel:', len(barrel_strands))
            if len(barrel_strands) > 0: # protein contains a barrel
                analysis = self.barrel_analysis(BM, barrel_strands, strand_list, protein_structure, protein_residue_list, protein_id)
                mtj_df = mtj_df.append(analysis, sort=False)        
            else: # protein contains a sheet
                analysis = self.sheet_analysis(strand_list, protein_structure, protein_residue_list, protein_id)      
                mtj_df = mtj_df.append(analysis, sort=False)                              
                                        
        print("FINISHED STRUCTURE(S)")

        columns = list(mtj_df.columns.values)
        mtj_df.to_csv(Path('Results/{} output_file_newdef.csv'.format(filename)), columns=columns, index=False)
   
        print(mtj_df)  
    
    def barrel_analysis(self, BM, barrel_strands, strand_list, protein_structure, protein_residue_list, protein_id):
        '''method used to gather information on barrel specific information'''
        pdbp = PDB_prep()
        axis_points = BM.centroid_determination(barrel_strands) #returns a list with endpoints of the axis
        axis_midpoint = BM.midpoint()
        # BARREL ROTATIONS AND TRANSLATIONS
        #    ROTATE to align with z-axis
        z_axis = Vector([0,0,1])
        unit_barrel_axis = Vector(np.reshape(BM.normalise_vector(np.subtract(axis_points[1], axis_points[0])),3))
        barrel_rotation_matrix = BM.get_barrel_rotation(unit_barrel_axis, z_axis)
        # print("transform")
        protein_structure = BM.transform(barrel_rotation_matrix, axis_midpoint, protein_structure)
        # pdbp.save_structure(protein_structure, protein_id+'transformed')
        #    FLIP
        if BM.check_correct_orientation() == False:
            print(BM.check_correct_orientation())
            # z_flip_matrix = np.array([[1, 0, 0], [0, 1, 0], [0, 0, -1]])
            # z_flip_axis = Vector([0,0,-1])
            matrix_180 = np.array([[-1,0,0],[0,1,0],[0,0,-1]])
            identity_matrix = np.array([[1,0,0],[0,1,0],[0,0,1]])

            #ROTATION AND TRANSLATION
            BM = BarrelManipulation.BarrelManipulation(protein_structure, protein_residue_list, strand_list)
            barrel_strands = BM.barrel_determination()
            axis_points = BM.centroid_determination(barrel_strands)  # returns a list with endpoints of the axis
            unit_barrel_axis = Vector(np.reshape(BM.normalise_vector(np.subtract(axis_points[1], axis_points[0])),3))
            # barrel_rotation_matrix = BM.get_barrel_rotation(unit_barrel_axis, -z_axis)
            midpoint = (axis_points[0]+axis_points[1])/2
            protein_structure = BM.transform(matrix_180, midpoint, protein_structure) #flips the structure along the x-axis
           
            # BM = BarrelManipulation.BarrelManipulation(protein_structure, protein_residue_list, strand_list)
            # barrel_strands = BM.barrel_determination()
            # axis_points = BM.centroid_determination(barrel_strands)  # returns a list with endpoints of the axis
            # unit_barrel_axis = Vector(np.reshape(BM.normalise_vector(np.subtract(axis_points[1], axis_points[0])),3))
            # midpoint = (axis_points[0]+axis_points[1])/2
            # print('mid', midpoint)
            # protein_structure = BM.transform(identity_matrix, midpoint, protein_structure) #transforms the structure back to the origin
          
        # print("flipped")
        pdbp.save_structure(protein_structure, protein_id+'flipped')
        BM = BarrelManipulation.BarrelManipulation(protein_structure, protein_residue_list, strand_list)
        barrel_strands = BM.barrel_determination()
        axis_points = BM.centroid_determination(barrel_strands)  # returns a list with endpoints of the axis
        SA = StrandAllocation.StrandAllocation(protein_id, protein_structure, protein_residue_list)
        ps = ProteinStatistics(protein_structure, protein_residue_list)
        if len(barrel_strands) >= 2:
            mtj = MTJSearch.MTJSearch(strand_list) 
            bs = BarrelStatistics.BarrelStatistics(axis_points, protein_structure, protein_residue_list)
            pi = PDBInfo.PDBInfo(protein_structure, protein_id)
            aromatics_in_protein = mtj.aromatic_list()
            if len(aromatics_in_protein) > 0:
                temp_mtj_df = pd.DataFrame()
                protein_weight = ps.protein_weight()
                structure_resolution = pi.protein_resolution()
                protein_name = pi.protein_name()
                protein_family = pi.protein_family()
                quaternary = pi.quaternary_structure()
                elucidation_method = pi.elucidation_method()
                for aromatic in aromatics_in_protein:
                    neighbouring_gly = mtj.check_for_gly(aromatic)
                    if neighbouring_gly != False: #aromatic neighbours a glycine
                        print(neighbouring_gly)
                        for glycine in neighbouring_gly:
                            #TODO check that the expanded glycine radius search produces false MTJs that look correct (pymol task).
                            #get all data
                            in_out_arom = bs.inner_outer_determination(aromatic)
                            in_out_gly = bs.inner_outer_determination(glycine)
                            if in_out_arom != in_out_gly:
                                continue
                            else:
                                print(aromatic)
                                temporary_df = pd.DataFrame(index=[0])
                                num_strands_in_barrel = len(barrel_strands)
                                arom_residue_name = aromatic.get_resname()
                                arom_residue_num = aromatic.get_full_id()[3][1]
                                gly_residue_num = glycine.get_full_id()[3][1]
                                aromatic_phi, aromatic_psi = SA.get_phipsi(aromatic)
                                glycine_phi, glycine_psi = SA.get_phipsi(glycine)
                                vertical_placement = bs.vertical_placement(glycine)
                                AN_residue, AC_residue = ps.res_adj_AAs(aromatic)
                                GN_residue, GC_residue = ps.res_adj_AAs(glycine)
                                rear_residue = None
                                rear_residue_list = bs.search_for_rear_residue(aromatic)
                                for residue in rear_residue_list:
                                    if residue != None:
                                        if bs.inner_outer_determination(residue) != in_out_arom:
                                            continue
                                        else:
                                            rear_residue = residue
                                if rear_residue != None:
                                    rear_residue_name = rear_residue.get_resname()
                                    rear_residue_num = rear_residue.get_full_id()[3][1]
                                    RN_residue, RC_residue = ps.res_adj_AAs(rear_residue)
                                else:
                                    rear_residue_name = "None"
                                    rear_residue_num = "None"
                                    RN_residue, RC_residue = "None", "None"
                                if len(barrel_strands) > 0:
                                    contains_barrel = True
                                else:
                                    contains_barrel = False
                                correct_torsion, chi1, chi2 = mtj.check_torsion(aromatic)
                                # print(chi1)
                                if correct_torsion == "BROKEN":
                                    continue
                                rca = ClashAnalysis(protein_structure, aromatic, chi1)
                                clash_analysis_df = rca.main()
                                temporary_df['Protein_id'] = protein_id
                                temporary_df['elucidation_method'] = elucidation_method
                                temporary_df['Resolution'] = structure_resolution
                                temporary_df['species'] = self.species
                                temporary_df['protein_weight'] = protein_weight
                                temporary_df['protein_name'] = self.protein_name
                                temporary_df['protein_family'] = protein_family
                                temporary_df['quaternary'] = quaternary
                                temporary_df['contains_barrel'] = contains_barrel
                                temporary_df['num_strands_in_barrel'] = num_strands_in_barrel
                                temporary_df['arom_residue_name'] = arom_residue_name
                                temporary_df['arom_residue_num'] = arom_residue_num
                                temporary_df['gly_residue_num'] = gly_residue_num
                                temporary_df['aromatic_phi'] = aromatic_phi
                                temporary_df['aromatic_psi'] = aromatic_psi
                                temporary_df['glycine_phi'] = glycine_phi
                                temporary_df['glycine_psi'] = glycine_psi
                                temporary_df['chi1'] = chi1
                                temporary_df['chi2'] = chi2
                                temporary_df['native_chi1_rotamer'] = rca.current_rotamer(chi1)
                                temporary_df['vertical_placement'] = vertical_placement
                                if (aromatic['O']-glycine['N']) < 3.5: # tries to determine if the residues are hydrogen bonded by measuring the distance between the oxygen and nitrogen of the two residues
                                    temporary_df['hydrogen_bonded'] = 1
                                else:
                                    temporary_df['hydrogen_bonded'] = 0
                                temporary_df['AN_residue'] = AN_residue
                                temporary_df['AC_residue'] = AC_residue
                                temporary_df['GN_residue'] = GN_residue
                                temporary_df['GC_residue'] = GC_residue
                                temporary_df['rear_residue_name'] = rear_residue_name
                                temporary_df['rear_residue_num'] = rear_residue_num
                                temporary_df['RN_residue'] = RN_residue
                                temporary_df['RC_residue'] = RC_residue
                                temporary_df = pd.concat([temporary_df, clash_analysis_df], axis=1, join='outer')
                                A_G_dist = mtj.distance_measure(aromatic, glycine)
                                temporary_df['Distance b/w residues'] = A_G_dist
                                      
                                # if A_G_dist <= 5 and 80<abs(chi2)<100: #Fergus' suggested definition of a MTJ
                                # if 45<chi1<75 and 80<abs(chi2)<100: #Original definition of the MTJ
                                # if 45<chi1<75 and 80<abs(chi2)<100 and A_G_dist <= 4.1: #DEFINITION MATT AND FERGUS STARTED WITH
                                if 47<chi1<73 and 80<abs(chi2)<100 and A_G_dist <= 4.5:    
                                    # aromatic is true mtj
                                    print('accepted as true')
                                    temporary_df['true/false'] = 1
                                    temp_mtj_df =temp_mtj_df.append(temporary_df, sort=False, ignore_index=True)                                   
                                else:
                                    #is a false mtj
                                    print("rejected as false")
                                    temporary_df['true/false'] = 0
                                    temp_mtj_df = temp_mtj_df.append(temporary_df, sort=False, ignore_index=True)
                return temp_mtj_df
    
    def sheet_analysis(self, strand_list, protein_structure, protein_residue_list, protein_id):
        '''method used to gather information on beta-sheet specific information'''
        print("Sheet analysis activated")
        print(len(strand_list))
        #TODO rewrite in_out_determination to be able to perform without a central axis, just needs to be able to show that
        #     the amino acids are facing in the same direction. maybe use something about CB atoms.
        SA = StrandAllocation.StrandAllocation(protein_id, protein_structure, protein_residue_list)
        mtj = MTJSearch.MTJSearch(strand_list)
        ps = ProteinStatistics(protein_structure, protein_residue_list)
        pi = PDBInfo.PDBInfo(protein_structure, protein_id)
        aromatics_in_protein = mtj.aromatic_list()
        if len(aromatics_in_protein) > 0:
            temp_mtj_df = pd.DataFrame()
            protein_weight = ps.protein_weight()
            structure_resolution = pi.protein_resolution()
            protein_name = pi.protein_name()
            protein_family = pi.protein_family()
            quaternary = pi.quaternary_structure()
            elucidation_method = pi.elucidation_method()
            for aromatic in aromatics_in_protein:
                neighbouring_gly = mtj.check_for_gly(aromatic)
                if neighbouring_gly != False: # aromatic neighbours a glycine
                    print(neighbouring_gly)
                    for glycine in neighbouring_gly:
                        #get all data
                        print(aromatic)
                        temporary_df = pd.DataFrame(index=[0])
                        num_strands_in_barrel = "NA"
                        arom_residue_name = aromatic.get_resname()
                        arom_residue_num = aromatic.get_full_id()[3][1]
                        gly_residue_num = glycine.get_full_id()[3][1]
                        aromatic_phi, aromatic_psi = SA.get_phipsi(aromatic)
                        glycine_phi, glycine_psi = SA.get_phipsi(glycine)
                        vertical_placement = None
                        AN_residue, AC_residue = ps.res_adj_AAs(aromatic)
                        GN_residue, GC_residue = ps.res_adj_AAs(glycine)
                        rear_residue = None
                        rear_residue_name = "NA" # Don't have reliable metric to figure out what the 'rear' residue is on a sheet
                        rear_residue_num = "NA"
                        RN_residue, RC_residue = "NA", "NA"
                        contains_barrel = False
                        correct_torsion, chi1, chi2 = mtj.check_torsion(aromatic)
                        if correct_torsion == "BROKEN":
                            continue
                        rca = ClashAnalysis(protein_structure, aromatic, chi1)
                        clash_analysis_df = rca.main()
                        temporary_df['Protein_id'] = protein_id
                        temporary_df['elucidation_method'] = elucidation_method
                        temporary_df['Resolution'] = structure_resolution
                        temporary_df['species'] = self.species
                        temporary_df['protein_weight'] = protein_weight
                        temporary_df['protein_name'] = self.protein_name
                        temporary_df['protein_family'] = protein_family
                        temporary_df['quaternary'] = quaternary
                        temporary_df['contains_barrel'] = contains_barrel
                        temporary_df['num_strands_in_barrel'] = num_strands_in_barrel
                        temporary_df['arom_residue_name'] = arom_residue_name
                        temporary_df['arom_residue_num'] = arom_residue_num
                        temporary_df['gly_residue_num'] = gly_residue_num
                        temporary_df['aromatic_phi'] = aromatic_phi
                        temporary_df['aromatic_psi'] = aromatic_psi
                        temporary_df['glycine_phi'] = glycine_phi
                        temporary_df['glycine_psi'] = glycine_psi
                        temporary_df['chi1'] = chi1
                        temporary_df['chi2'] = chi2
                        temporary_df['native_chi1_rotamer'] = rca.current_rotamer(chi1)
                        temporary_df['vertical_placement'] = vertical_placement
                        strand_distance = aromatic['O']-glycine['N']
                        if strand_distance < 3.5:
                            temporary_df['hydrogen_bonded'] = 1
                        else:
                            temporary_df['hydrogen_bonded'] = 0
                        temporary_df['AN_residue'] = AN_residue
                        temporary_df['AC_residue'] = AC_residue
                        temporary_df['GN_residue'] = GN_residue
                        temporary_df['GC_residue'] = GC_residue
                        temporary_df['rear_residue_name'] = rear_residue_name
                        temporary_df['rear_residue_num'] = rear_residue_num
                        temporary_df['RN_residue'] = RN_residue
                        temporary_df['RC_residue'] = RC_residue
                        temporary_df = pd.concat([temporary_df, clash_analysis_df], axis=1, join='outer')
                        A_G_dist = mtj.distance_measure(aromatic, glycine)
                        temporary_df['Distance b/w residues'] = A_G_dist
                        if A_G_dist <= 5 and 80<abs(chi2)<100:
                            # aromatic is true mtj
                            print('accepted as true')
                            temporary_df['true/false'] = 1
                            temp_mtj_df =temp_mtj_df.append(temporary_df, sort=False)                                   
                        else:
                            #is a false mtj
                            print("rejected as false")
                            temporary_df['true/false'] = 0
                            temp_mtj_df =temp_mtj_df.append(temporary_df, sort=False)
            return temp_mtj_df
                                
    
if __name__ == "__main__":
    # with open(input("Enter file name: "), 'r') as csvfile:
    input_file = "RCSB_Barrel_3" #"PDB_FH_revised"
    path = Path("Inputs/{}.csv".format(input_file))
    with open(path, 'r') as csvfile:
        reader = csv.reader(csvfile)
        csv_list = list(reader)
        print(path.stem)
    mtj = MTJFinder()
    MTJFinder.iterate_list(mtj, csv_list, input_file)
