import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns
from sklearn import neighbors
from sklearn.metrics import mean_absolute_error, accuracy_score
from sklearn import model_selection
from sklearn.linear_model import LogisticRegression
from sklearn.feature_selection import RFECV

def setup(file):
    train = pd.read_csv(file)

    return train


def split_data(train, to_predict):
    from sklearn.model_selection import train_test_split
    y = train[to_predict]
    train.drop([to_predict], axis=1, inplace=True)
    X = train
    train_x, test_x, train_y, test_y = train_test_split(X,y,test_size = .33, random_state = 0)
    return train_x, test_x, train_y, test_y

def logreg(train_x, test_x, train_y, test_y, all_x, all_y):
    logreg = LogisticRegression(solver='liblinear')
    logreg.fit(train_x,train_y)
    y_pred = logreg.predict(test_x)
    accuracy = round(accuracy_score(y_pred, test_y),4)
    cv_scores = model_selection.cross_val_score(LogisticRegression(solver='liblinear'), all_x, all_y, cv=10 )
    data = {'model':'logreg', 'Accuracy':accuracy, 'CV score avg': cv_scores.mean(), 'CV std':cv_scores.std()}
    df = pd.DataFrame(data=data, index=[0])
    return df

def logreg2(train_x, test_x, train_y, test_y, all_x, all_y):
    estimator_name = "LogReg"
    model = LogisticRegression(solver='liblinear')
    try:
        selector = RFECV(model, step=1, cv=5)
        fit = selector.fit(train_x, train_y)
        # cv_scores = model_selection.cross_val_score(LogisticRegression(solver='liblinear'), all_x, all_y, cv=10 )
        selected_features = pd.DataFrame(data={'features':train_x.columns, 'used':fit.support_}).loc[selected['used']==True]
        useful_features = selected['features'].apply(lambda x: x.split('_')[0]).unique() #so that dummy variables can be grouped
    except:
        for each feature in the dataset
            remove the feature from the dataset
            test the cv scores of the new dataset
            compare the average score with the average score of the full dataset
            add the score to a dataframe with a row indicator of the removed feature
            if the average score (accuracy) has decreased because the feature was removed:
                the feature was important, add an important tag to the table
            elif the average score was increased because the feature was removed:
                the feature was not important, add a 'detrimental' tag to the table
        #Now you have a table which contains the score, the change in score from the standard, and whether it increases or decreases the score by having it in the data.
        Pick the top 10% of the features (metric to be decided, can be changed) and make a dataset using those features.
        train the model on that dataset
        test the model on the set aside test set.
        model.fit(train_x,train_y)
        data = {'model':'logreg', 'Accuracy':accuracy, 'CV score avg': cv_scores.mean(), 'CV std':cv_scores.std()}
        df = pd.DataFrame(data=data, index=[0])


    # 1. RFECV the data to remove useless features.
    # 2. cross validate the data one the removed features
    # 2a. if features cannot be removed do a normal CV on all the features and make a note that features were not eliminated

def logreghyperparamtertuning(hyperparameter):
    from scipy.stats import uniform as sp_rand
    from sklearn.model_selection import RandomizedSearchCV
    # prepare a uniform distribution to sample for the alpha parameter
    param_grid = {'alpha': sp_rand()}
    # create and fit a ridge regression model, testing random alpha values
    model = Ridge()
    rsearch = RandomizedSearchCV(estimator=model, param_distributions=param_grid, n_iter=100)
    rsearch.fit(dataset.data, dataset.target)
    print(rsearch)
    # summarize the results of the random parameter search
    print(rsearch.best_score_)
    print(rsearch.best_estimator_.alpha)


                        
train, test = setup("train.csv")

#ONE HOT ENCODING
columns = ["Pclass",'Embarked', 'Title', 'Agebin']
train = pd.get_dummies(train, columns=columns, drop_first=False)
test = pd.get_dummies(test, columns=columns, drop_first=False)

train_x, test_x, train_y, test_y = split_data(train.copy(deep=True), 'Survived')
all_y = train.pop('Survived')
all_x = train

classifiers = {0:logreg,
               1:gaussNB,
               2:SVMclass,
               3:decisionTree,
               4:Xgboost,
               5:RandForest,
               6:NeuralNetClassify,
               7:nearestNeighbour}
summary_df = pd.DataFrame()
for i in range(0,7):
    results = classifiers[i](train_x, test_x, train_y, test_y, all_x, all_y, test)
    summary_df = summary_df.append(results, sort=True)
summary_df = summary_df[['model', 'Accuracy', 'CV score avg', 'CV std']]
print(summary_df)

