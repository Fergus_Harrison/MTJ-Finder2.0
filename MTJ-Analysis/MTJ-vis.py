import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

def nat_score(x):
    # find the lowest record score column value
    # print(x)
    nat_rot = x.at['native_chi1_rotamer']
    # add 'score' to the end of the value
    nat_rot = nat_rot+'score'
    # return the entry that corresponds with 'value+score'
    return x.at[nat_rot]

def low_score(x):
    # find the lowest record score column value
    low_rot = x['lowest_rotamer_energy']
    # add 'score' to the end of the value
    low_rot = low_rot+'score'
    # return the entry that corresponds with 'value+score'
    return x.at[low_rot]

def score_lead(x):
    y =x[['+gauchescore', 'transscore', '-gauchescore']]
    y.sort_values(inplace=True)
    # print(y)
    # print(y.iloc[1])
    lead = y.iloc[1]-x['lowest_score']
    # print(lead)
    return lead


# mtj_df = pd.read_csv('MTJ_tuning output_file_newdef.csv', delimiter=',',index_col=0).reset_index(drop=True)
mtj_df = pd.read_csv('MTJ_tuning 2.csv', delimiter=',',index_col=0).reset_index(drop=True)
print(mtj_df.columns)
keep_list = ['Protein_id',
       'arom_residue_name',
       'aromatic_phi', 'aromatic_psi',
       'glycine_phi', 'glycine_psi', 'chi1', 'chi2', 'native_chi1_rotamer',
       'vertical_placement', 'hydrogen_bonded', 'AN_residue', 'AC_residue',
       'GN_residue', 'GC_residue', 'rear_residue_name',
       'RN_residue', 'RC_residue', '+gauchescore',
       'transscore', '-gauchescore',
       'lowest_rotamer_energy', 'Distance b/w residues', 'true/false']
label_list = ['arom_residue_name', 'native_chi1_rotamer',
       'AN_residue', 'AC_residue', 'GN_residue', 'GC_residue', 
       'rear_residue_name', 'rear_residue_num', 'RN_residue', 'RC_residue']
mtj_df = mtj_df[keep_list]
mtj_df['native_score'] = mtj_df.apply(lambda x: nat_score(x), axis=1)
mtj_df['lowest_score'] = mtj_df.apply(lambda x: low_score(x), axis=1)
mtj_df['low_score_lead'] = mtj_df.apply(lambda x: score_lead(x), axis=1)
# mtj_df['low_score_lead'] = mtj_df['low_score_lead'][mtj_df['low_score_lead'] < 100000]
mtj_df['low_score_lead'] = np.log(mtj_df['low_score_lead'])
print(mtj_df['low_score_lead'])
# sns.scatterplot(x="native_score", y="lowest_score", hue="true/false", data=mtj_df)
# plt.show()
if False:
    sns.scatterplot(x=range(len(mtj_df)), y="native_score", hue="true/false", data=mtj_df)
    plt.show()
if False:
    correlation = mtj_df.corr().round(decimals=1)
    heatmap = sns.heatmap(data=correlation, annot=True)
    heatmap.figure.subplots_adjust(bottom = 0.3)
    plt.show()
if False:
    sns.scatterplot(x=range(len(mtj_df)), y="low_score_lead", hue="true/false", data=mtj_df)
    plt.show()
if True:
    pairplot = sns.pairplot(data=mtj_df, hue='true/false', diag_kind='hist', height=1, aspect=0.5)
    plt.show()