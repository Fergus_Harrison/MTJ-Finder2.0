# take in the csv file as a pandas Dataframe
# Add a column called native_rotamer and fill it with categorical of what the native rotamer is called.
# Add a column called lowest_energy_rotamer_score and fill it with the name of the rotamer that had the lowest clash score

import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

def collect_and_combine():
    false_first_df = pd.read_csv('output_file_false PDB_FH_revised.csv', delimiter=',',index_col=0).reset_index(drop=True)
    true_first_df = pd.read_csv('output_file_true PDB_FH_revised.csv', delimiter=',', index_col=0).reset_index(drop=True)
    false_first_df['true/false'] = 0
    true_first_df['true/false'] = 1
    combined_df = false_first_df.append(true_first_df, sort=False)

    # false_second_df = pd.read_csv('false_output_file_newdef second.csv', delimiter=',',index_col=0).reset_index(drop=True)
    # true_second_df = pd.read_csv('true_output_file_newdef second.csv', delimiter=',', index_col=0).reset_index(drop=True)
    # false_second_df['true/false'] = 0
    # true_second_df['true/false'] = 1
    # second_df = false_second_df.append(true_second_df, sort=False)

    # combined_df = first_df.append(second_df, sort=False)
    print(combined_df.columns)
    print(combined_df.head())
    print(combined_df.info())
    combined_df.to_csv('olddef_combined.csv')

def explore():
    #explore the data in the newdef 
    # Load csv and one-hot encode the categorical data
    olddef_df = pd.read_csv('olddef_combined.csv', delimiter=',',index_col=0).reset_index(drop=True)
    cols_of_interest = ['Protein_id', 'arom_residue_name', 'aromatic_phi', 'aromatic_psi', 'glycine_phi', 
                        'glycine_psi', 'chi1', 'chi2', 'native_chi1_rotamer', 'vertical_placement', 
                        '+gauchescore', '-gauchescore', 'transscore', 'lowest_rotamer_energy', 'true/false',
                        'Distance b/w residues']
    AA_mapping = {'ALA':0, 'ALAD':1, 'ARG':2, 'ASN':3, 'ASP':4, 'CYS':5, 'GLN':6, 'GLU':7, 'GLY':8, 'HSD':9, 
                'HSE':10, 'HSP':11, 'ILE':12, 'LEU':13, 'LYS':14, 'MET':15, 'PHE':16, 
                'PRO':17, 'SER':18, 'THR':19, 'TRP':20, 'TYR':21, 'VAL':22}
    rot_mapping = {'+gauche':1.0, 'trans':2.0, '-gauche':3.0}
    rot_columns = ['native_chi1_rotamer', 'lowest_rotamer_energy']
    res_columns = ['arom_residue_name']
    olddef_df = olddef_df[cols_of_interest]
    olddef_df['lowest_rotamer_energy'] = olddef_df['lowest_rotamer_energy'].map(lambda x: x[:-5])
    olddef_df[res_columns] = olddef_df[res_columns].applymap(AA_mapping.get)
    olddef_df[rot_columns] = olddef_df[rot_columns].applymap(rot_mapping.get)

    print('true mtjs native rotamer description\n', olddef_df.loc[olddef_df['true/false']==1, 'native_chi1_rotamer'].describe())
    print(olddef_df.loc[olddef_df['true/false']==1, 'native_chi1_rotamer'].unique())
    print(olddef_df.loc[(olddef_df['native_chi1_rotamer'] > 1) & (olddef_df['true/false']==1)].head())

collect_and_combine()
# filter_df = full_df[['native_chi1_rotamer', 'lowest_rotamer_energy', 'true/false']]
# sns.lmplot( "native_chi1_rotamer", "lowest_rotamer_energy", data=filter_df, hue='true/false', fit_reg=False)
# plt.show()
# full_df = full_df.astype('float64', errors='ignore')
# corr_true = full_df.loc[full_df['true/false']==1].drop('true/false', axis=1).corr()
# corr_false = full_df.loc[full_df['true/false']==0].drop('true/false', axis=1).corr()
# print(full_df['native_chi1_rotamer'].loc[full_df['true/false']==1].describe())
# print(full_df['lowest_rotamer_energy'].loc[full_df['true/false']==1].describe())

# # print(full_df.info())
# ax = sns.heatmap(corr_true, xticklabels=corr_true.columns, yticklabels=corr_true.columns, annot=True)
# ax.set_title('true_mtjs')
# ax.figure.subplots_adjust(bottom = 0.3)
# plt.show()
# ax = sns.heatmap(corr_false, xticklabels=corr_false.columns, yticklabels=corr_false.columns, annot=True)
# ax.set_title('false_mtjs')
# ax.figure.subplots_adjust(bottom = 0.3)
# plt.show()
