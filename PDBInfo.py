import os
from pathlib import Path
import SCOPe_parser


class PDBInfo:
    def __init__(self, structure, structure_id):
        self.structure = structure
        self.scop = SCOPe_parser.SCOPe_parser(structure_id)
        directory = os.path.dirname(__file__)
        self.dirname = Path('./raw_PDB/pdb{}.ent'.format(structure_id))

    def protein_name(self):
        '''A method that returns the name of the given protein. It first
        searches for the name using the Scope protein database, if that fails
        it will then use the PDB file header.'''
        #first try Scope
        try:
            name_of_protein = self.scop.name
            return name_of_protein
        #then try PDB
        except:
            with open(self.dirname, 'r') as PDB_txt:
                for line in PDB_txt.readlines():
                    list = line.split()
                    type = list[0]
                    if type == "COMPND":
                        if list[1] == "2":
                            for x, element in enumerate(list):
                                if element == "MOLECULE:":
                                    protein_name = ' '.join(list[x + 1:])
                                    return protein_name
                    if type == "ATOM":
                        print("reached ATOM records")
                        break
                print("read through whole file, couldn't find reference to name")
                return "Not Found"
            return name_of_protein

    def protein_resolution(self):
        '''fetches the resolution of the crystal structure by using the declared 
        resolution in the PDB header'''
        #first try pdb header parser from biopython
        try:
            resolution = self.structure['resolution']
            return resolution
        #then try manually sorting through the pdb 
        except:
            with open(self.dirname, 'r') as PDB_txt:
                for line in PDB_txt.readlines():
                    list = line.split()
                    type = list[0]
                    if type == "REMARK":
                        if list[1] == "2":
                            if len(list) >=4:
                                if list[2] == 'RESOLUTION.':
                                    resolution = list[3]
                                    return resolution
                                else:
                                    continue
                    if type == "ATOM":
                        print("reached ATOM records")
                        print("read through whole file, couldn't find reference to resolution")
                        return "Not Found"

    def elucidation_method(self):
        '''method used to get structure, NMR vs XRAY by reading the PDB header'''
        #first try PDB header
        try:
            method = self.structure['structure_method']
        except:
            with open(self.dirname, 'r') as PDB_txt:
                for line in PDB_txt.readlines():
                    list = line.split()
                    type = list[0]
                    if type == "EXPDTA":
                        method = ' '.join(list[1:])
                        return method
                    if type == "ATOM":
                        print("reached ATOM records")
                        break
                print("read through whole file, couldn't find reference to method")
                return "Not Found"
        return method

    def species_origin(self):
        '''Returns the species of origin of the protein 
        (as opposed to the expression organisms), as read from the PDB header'''
        with open(self.dirname, 'r') as PDB_txt:
            for line in PDB_txt.readlines():
                list = line.split()
                type = list[0]
                if type == "SOURCE":
                    if list[1] == "2":
                        for x, element in enumerate(list):
                            if element == "ORGANISM_SCIENTIFIC:":
                                organism = ' '.join(list[x+1:])
                                return organism
                if type == "ATOM":
                    print("reached ATOM records")
                    break
            print("read through whole file, couldn't find reference to species")
            return "Not Found"

    def quaternary_structure(self):
        '''Returns the quaternary structure of the protein as described by the PDB file.'''
        with open(self.dirname, 'r') as PDB_txt:
            for line in PDB_txt.readlines():
                list = line.split()
                type = list[0]
                if type =="REMARK":
                    if list[1]=="350":
                        if len(list) >= 7:
                            if list[4] == 'QUATERNARY':
                                return list[6]
                            if list[4] == 'BIOLOGICAL':
                                 return list[6]
                if type == "ATOM":
                    print("reached ATOM records")
                    break
            print("read through whole file, couldn't find reference to quaternary structure")
            return "Not Found"

    def protein_family(self):
        '''returns the family of protein that the protein belongs to'''
        return self.scop.family



# Basic INFO of protein (only IF MTJ has been found in protein):
# PDB Header reader
# Check protein name
# If not, refer to Scope database
# Check protein resolution
# Check elucidation method
# Check species of original organism protein is found in.
# Check quaternary structure
# If not, refer to Scope Database
# Scope database reader/referencer
# Check protein family
# Calculate protein size in KDa
# Either call a library or write own, based off size of each amino acid.
