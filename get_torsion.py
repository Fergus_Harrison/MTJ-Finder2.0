# Copyright (c) 2014 Lenna X. Peterson, all rights reserved
# lenna@purdue.edu

import logging
import math
from numpy import array, cross

# Package Bio can be obtained from http://www.biopython.org
from Bio import PDB
import ProteinStatistics as ps

logging.basicConfig(level=logging.DEBUG)

chi_atoms = dict(
    chi1=dict(
        ARG=['N', 'CA', 'CB', 'CG'],
        ASN=['N', 'CA', 'CB', 'CG'],
        ASP=['N', 'CA', 'CB', 'CG'],
        CYS=['N', 'CA', 'CB', 'SG'],
        GLN=['N', 'CA', 'CB', 'CG'],
        GLU=['N', 'CA', 'CB', 'CG'],
        HIS=['N', 'CA', 'CB', 'CG'],
        ILE=['N', 'CA', 'CB', 'CG1'],
        LEU=['N', 'CA', 'CB', 'CG'],
        LYS=['N', 'CA', 'CB', 'CG'],
        MET=['N', 'CA', 'CB', 'CG'],
        PHE=['N', 'CA', 'CB', 'CG'],
        PRO=['N', 'CA', 'CB', 'CG'],
        SER=['N', 'CA', 'CB', 'OG'],
        THR=['N', 'CA', 'CB', 'OG1'],
        TRP=['N', 'CA', 'CB', 'CG'],
        TYR=['N', 'CA', 'CB', 'CG'],
        VAL=['N', 'CA', 'CB', 'CG1'],
    ),
    altchi1=dict(
        VAL=['N', 'CA', 'CB', 'CG2'],
    ),
    chi2=dict(
        ARG=['CA', 'CB', 'CG', 'CD'],
        ASN=['CA', 'CB', 'CG', 'OD1'],
        ASP=['CA', 'CB', 'CG', 'OD1'],
        GLN=['CA', 'CB', 'CG', 'CD'],
        GLU=['CA', 'CB', 'CG', 'CD'],
        HIS=['CA', 'CB', 'CG', 'ND1'],
        ILE=['CA', 'CB', 'CG1', 'CD1'],
        LEU=['CA', 'CB', 'CG', 'CD1'],
        LYS=['CA', 'CB', 'CG', 'CD'],
        MET=['CA', 'CB', 'CG', 'SD'],
        PHE=['CA', 'CB', 'CG', 'CD1'],
        PRO=['CA', 'CB', 'CG', 'CD'],
        TRP=['CA', 'CB', 'CG', 'CD1'],
        TYR=['CA', 'CB', 'CG', 'CD1'],
    ),
    altchi2=dict(
        ASP=['CA', 'CB', 'CG', 'OD2'],
        LEU=['CA', 'CB', 'CG', 'CD2'],
        PHE=['CA', 'CB', 'CG', 'CD2'],
        TYR=['CA', 'CB', 'CG', 'CD2'],
    ),
    chi3=dict(
        ARG=['CB', 'CG', 'CD', 'NE'],
        GLN=['CB', 'CG', 'CD', 'OE1'],
        GLU=['CB', 'CG', 'CD', 'OE1'],
        LYS=['CB', 'CG', 'CD', 'CE'],
        MET=['CB', 'CG', 'SD', 'CE'],
    ),
    chi4=dict(
        ARG=['CG', 'CD', 'NE', 'CZ'],
        LYS=['CG', 'CD', 'CE', 'NZ'],
    ),
    chi5=dict(
        ARG=['CD', 'NE', 'CZ', 'NH1'],
    ),
)

def dihedral(vec1,vec2,vec3,vec4):
    """
    Returns a float value for the dihedral angle between 
    the four vectors. They define the bond for which the 
    torsion is calculated (~) as:
    V1 - V2 ~ V3 - V4 
    The vectors vec1 .. vec4 can be array objects, lists or tuples of length 
    three containing floats. 
    
    If the dihedral angle cant be calculated (because vectors are collinear),
    the function raises a DihedralGeometryError
    """    
    all_vecs = [vec1,vec2,vec3,vec4]
    # rule out that two of the atoms are identical
    # except the first and last, which may be.
    for i in range(len(all_vecs)-1):
        for j in range(i+1,len(all_vecs)):
            if i>0 or j<3: # exclude the (1,4) pair
                equals = all_vecs[i]==all_vecs[j]
                if equals.all():
                    raise DihedralGeometryError(\
                        "Vectors #%i and #%i may not be identical!"%(i,j))

    # calculate vectors representing bonds
    ab = vec2-vec1
    bc = vec3-vec2
    cd = vec4-vec3
    # calculate vectors perpendicular to the bonds
    normal1 = cross(ab,bc)
    normal2 = cross(bc,cd)
    # check for linearity
    if ps.magnitude(normal1) == 0 or ps.magnitude(normal2)== 0:
        raise DihedralGeometryError(\
            "Vectors are in one line; cannot calculate normals!")
    # normalize them to length 1.0
    normal1 = ps.normalise_vector(normal1)
    normal2 = ps.normalise_vector(normal2)
    # calculate torsion and convert to degrees
    torsion = math.degrees(ps.angle(normal1,normal2))
    # take into account the determinant
    # (the determinant is a scalar value distinguishing
    # between clockwise and counter-clockwise torsion.
    if sum(normal1*cd) >= 0:
        return -torsion
    else:
        return torsion
        # torsion = torsion
        if torsion == 360:
            torsion = 0.0
    

def calculate_torsion(chi, res):
    '''Calculate side-chain torsion angles for given residue'''
    chi_names = list()
    for x in chi: # generates a list of Chi angles that have been specified to calculate
        reg_chi = "chi{}".format(x)
        if reg_chi in chi_atoms.keys():  # checks if specified chi angle exists
            chi_names.append(reg_chi) # adds regular chi angle to a list of chi angles to be calculated
        else:
            logging.warning("Invalid chi {}".format(x))
    if res.id[0] != " ": # checks residue exists
        pass
    res_name = res.resname # gets the name of the given residue
    if res_name in ("ALA", "GLY"): # if the given residue is alanine or glycine it doesn't calculate it.
        return None
    chi_list = [""] * len(chi_names) # creates an empty list the length of the amount of chi angles to calculate
    for x, chi in enumerate(chi_names):
        chi_res = chi_atoms[chi] # chooses the necessary dictionary for the calculation
        atom_list = chi_res[res_name] # chooses the residue atoms to be used for calculation
        try:
            vec_atoms = [res[a] for a in atom_list] # creates a list of atoms in the residue to be calculated
        except:
            return None #returns None if the residue is incomplete/incorrectly labelled
        vectors = [a.get_vector() for a in vec_atoms]
        angle = PDB.calc_dihedral(*vectors)
        angle = math.degrees(angle)
        chi_list[x] = angle # adds the result to empty list
    return chi_list