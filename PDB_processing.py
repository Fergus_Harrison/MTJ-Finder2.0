import csv
import logging
import os
import warnings
from pathlib import Path

import numpy as np
import pandas as pd
from Bio import BiopythonWarning
from Bio.PDB import PDBIO, PDBList, PDBParser, Selection, Vector, Select, Atom

import BarrelManipulation
import BarrelStatistics
import MTJSearch
import PDBInfo
import StrandAllocation
from ProteinStatistics import ProteinStatistics
from RotamerClashAnalysis import ClashAnalysis

warnings.simplefilter('ignore', BiopythonWarning)
logging.getLogger('matplotlib').setLevel(logging.WARNING) 

accepted_residues = ['ALA', 'ALAD', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY','HIS', 'HSD', 
                     'HSE', 'HSP', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 
                     'TRP', 'TYR', 'VAL']
class OrderedOutput(Select):  # Inherit methods from Select class
    def accept_atom(self, atom):
        if atom.get_parent().get_resname().strip() in accepted_residues:
            if (not atom.is_disordered()) or atom.get_altloc() == 'A':
                atom.set_altloc(' ')  # Eliminate alt location ID before output.
                return True
            else:  # Alt location was not one to be output.
                return False
        else:
            return False

class PDB_prep():

    def __init__(self):
        self.accepted_residues = ['ALA', 'ALAD', 'ARG', 'ASN', 'ASP', 'CYS', 'GLN', 'GLU', 'GLY','HIS', 'HSD', 
                                  'HSE', 'HSP', 'ILE', 'LEU', 'LYS', 'MET', 'PHE', 'PRO', 'SER', 'THR', 
                                  'TRP', 'TYR', 'VAL']

    def fetch_structure(self, structure_id):
        """ Takes structure ID to download the corresponding pdb file."""
        PDBList().retrieve_pdb_file(structure_id, False, "raw_PDB", 'pdb') #downloads the file to directed subfolder "PDB"
        directory = os.path.dirname(__file__)
        dirname = Path('./raw_PDB/pdb{}.ent'.format(structure_id))
        structure = PDBParser().get_structure(structure_id, dirname) # Parses already downloaded file to be used
        io = PDBIO()
        io.set_structure(structure)
        io.save(str(Path('./Ordered_PDB/pdb{}.ent'.format(structure_id))), select=OrderedOutput()) # Save file without disordered residues, only takes the 'A' version of the residue
        Ordered_dirname = Path('./Ordered_PDB/pdb{}.ent'.format(structure_id))
        structure = PDBParser().get_structure(structure_id, Ordered_dirname)
        return structure

    def chain_only_structure(self, structure):
        '''returns only the first chain in a structure as a structure object,
        Also removes disordered residues, leaving the "A" residue behind'''
        for res in structure.get_residues():
            if res.is_disordered():
                print('found a disorderly residue, this could cause issues in the program,\n Please manually remove the disordered atom alternative coordinates.')
                print(res.get_full_id)
        chains = Selection.unfold_entities(structure, 'C')
        new_structure = chains[0]        
        return new_structure

    def save_structure(self, structure, filename, selectClass=None):
        '''saves structure as a .ent file into the main directory'''
        directory = os.path.dirname(__file__)
        # pdb_name = os.path.join(directory, 'Ordered_PDB', filename + ".ent")
        pdb_name = Path('./Ordered_PDB/{}.ent'.format(filename))
        io = PDBIO()
        io.set_structure(structure)
        io.save(pdb_name)
        return


        
