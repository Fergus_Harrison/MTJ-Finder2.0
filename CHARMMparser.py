# Module written to parse the forcefield and charge files provided by CHARMM into a pandas
# DataFrame, should allow for a object to specify an atom and for it to return the Rmin and Eps,
# or the atomic charge when requested. 
import os
from pathlib import Path
import numpy as np
import pandas as pd


class CHARMMparser():
    def __init__(self):
        # pull in both .rtf and .prm files into separate dataframes.
        script_dir = os.path.dirname(__file__)  # <-- absolute dir this file is in
        par_abs_file_path = Path("./CHARMM/par_all22_prot_edited.prm")
        top_abs_file_path = Path("./CHARMM/top_all22_prot_edited.rtf")
        # par_abs_file_path = os.path.join(script_dir, par_path)
        # top_abs_file_path = os.path.join(script_dir, top_path)
        self.forcefield_params = pd.read_csv(par_abs_file_path, delim_whitespace=True)
        self.atomic_charges = pd.read_csv(top_abs_file_path, delim_whitespace=True, header=None, names=['residue','atom','CHARMM_atom','charge'])
        self.forcefield_params.set_index('atom', inplace=True)
        self.atomic_charges.set_index(['residue', 'atom'], inplace=True)

    def get_charge(self, res_name, atom_name):
        '''Returns the atomic charge of a given atom in a 
        residue as according to the CHARMM calculated values'''
        if res_name == 'HIS':
            res_name = 'HSE'
        if atom_name == 'OXT':
            atom_name = 'O'
        charge = self.atomic_charges.at[(res_name,atom_name), "charge"]
        return charge
    
    def get_epsilon(self, res_name, atom_name):
        '''Returns the Epsilon value of a given atom in a residue, Epsilon in the geometric well depth of
        a lennard-jones potential graph for that atom'''
        charmm_atom = self.get_CHARMMatom(res_name, atom_name)
        eps = self.forcefield_params.at[charmm_atom, 'epsilon']
        return eps

    def get_Rmin2(self, res_name, atom_name):
        '''Returns the Rmin2 value of a given atom in a residue according to CHARMM paramter file.'''
        charmm_atom = self.get_CHARMMatom(res_name, atom_name)
        Rmin2 = self.forcefield_params.at[charmm_atom, 'Rmin/2']
        return Rmin2

    def get_CHARMMatom(self, res_name, atom_name):
        '''Returns the CHARMM equivalent name for particular atoms. Some atoms used by the PDB standard are
        not supported by CHARMM, this provides a translation between the two'''
        if res_name == 'HIS':
            res_name = 'HSE'
        if atom_name == 'OXT':
            atom_name = 'O'
        try:
            charmm_atom = self.atomic_charges.at[(res_name,atom_name), 'CHARMM_atom']
        except KeyError :
            print('One of the atoms could not be found in the CHARMM files')
            print('residue:', res_name, '\natom:', atom_name)
            raise
        return charmm_atom
    
    def unbonded_interaction(self, residue1, atom1, residue2, atom2, distance):
        '''Method responsible for putting together the mathematics recquired for calculating
        the unbonded atomic interaction between two atoms in terms of kCal/mole (Lennard-Jones definition)'''
        #https://www.charmmtutorial.org/index.php/The_Energy_Function
        #https://www.ch.embnet.org/MD_tutorial/pages/MD.Part2.html
        # V(Lennard-Jones) = Eps,i,j[(Rmin,i,j/ri,j)**12 - 2(Rmin,i,j/ri,j)**6]
        # 
        # epsilon: kcal/mole, Eps,i,j = sqrt(eps,i * eps,j)
        # Rmin/2: A, Rmin,i,j = Rmin/2,i + Rmin/2,j
        # V{electrostatic} = q{i}*q{j}/E*r
        # THEREFORE:
        # V(Unbonded) = V{lennard-Jones}+V{electrostatic}
                  
        E = 1 # the dielectric constant of the solvent (pretty much always one)
        try:
            atom1_Rmin2 = self.get_Rmin2(residue1, atom1)
            atom1_eps = self.get_epsilon(residue1, atom1)
            atom1_charge = self.get_charge(residue1, atom1)
            atom2_Rmin2 = self.get_Rmin2(residue2, atom2)
            atom2_eps = self.get_epsilon(residue2, atom2)
            atom2_charge = self.get_charge(residue2, atom2)
        except KeyError:
            print("keyerror "*15)
            return 0
        except:
            print("something went wrong collecting the values for calculating clashes")
            print(residue1, atom1, '\n'+residue2, atom2)
            raise
        
        Rmin_ij = atom1_Rmin2 + atom2_Rmin2 #should be equivalent to (RMINi+RMINj)/2
        Eps_ij = np.sqrt(atom1_eps*atom2_eps)

        atomic_distance_calc = (Rmin_ij/distance)**12 - 2*((Rmin_ij/distance)**6)
        V_lj = Eps_ij*atomic_distance_calc
        V_elec = (atom1_charge*atom2_charge)/(E*distance)
        V_unbonded = V_lj + V_elec
        if V_lj == np.nan:
            print("vdw energy", V_unbonded)
            print('energy is nan, could be be due to zero division')
            raise
        return V_unbonded

# if __name__ == "__main__":
#     ch = CHARMMparser()
#     ch.init()
#     charge = ch.get_charge('ARG', 'N')
#     eps = ch.get_epsilon('C')
#     Rmin2 = ch.get_Rmin2('C')
#     print(charge)
#     print(eps)
#     print(Rmin2)
