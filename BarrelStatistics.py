import numpy as np
from Bio.PDB import NeighborSearch
from Bio.PDB import Selection
from Bio.PDB import Vector
from Bio.SeqUtils.ProtParam import ProteinAnalysis
from ProteinStatistics import ProteinStatistics

class BarrelStatistics:

    def __init__(self, axis_points, structure, residue_list):
        self.structure = structure
        self.centroid1 = axis_points[0]
        self.centroid2 = axis_points[1]
        self.residue_list = residue_list
        self.ps = ProteinStatistics(structure, residue_list)
        self.AminoAcidLetters = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D',
                                 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N',
                                 'GLN':'Q', 'CYS':'C', 'SEC':'U', 'GLY':'G',
                                 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L',
                                 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y',
                                 'VAL':'V'}
   
    def inner_outer_determination(self, residue): #barrel specific
        '''Returns the inner and outer 'facing' of a residue in a barrel,
        primarily used to check that both MTJ residues are 'facing' the same direction
        i.e. facing in towards the centre of the barrel or facing out away from the centre.
        facing in = internal
        facing out = external'''
        directional_vector = self.ps.directional_vector(residue)
        barrel_axis_vector = Vector(np.subtract(self.centroid1,self.centroid2))
        angle = self.vector_to_axis_angle(directional_vector, barrel_axis_vector)
        if angle < 90:
            return "internal"
        else:
            return "external"

    def vector_to_axis_angle(self, vector1, vector2): #barrel specific
        '''returns the angle between two vectors, used normally to find the angle between a
        residue atom and the barrel axis'''
        angle_between = self.ps.angle(vector1,vector2)
        return angle_between

    def midpoint(self, nitrogen, carbon): #protein agnostic
        #CURRENTLY UNUSED
        '''returns the midpoint between a nitrogen and a carbon atom'''
        nit = nitrogen.get_coord()
        carb = carbon.get_coord()
        x = (nit[0]+carb[0])/2
        y = (nit[1]+carb[1])/2
        z = (nit[2]+carb[2])/2
        midpoint = np.array([x,y,z])
        return midpoint

    def vertical_placement(self, glycine): #barrel specific
        '''returns the z-axis coordinate of the glycine CA atom in a MTJ. 
        so that you can determine the relative position of the MTJ in the barrel.
        Assumes that structure is centred at origin.'''
        z_coord = glycine["CA"].get_coord()[2]
        return z_coord

    def search_for_rear_residue(self, residue): #protein agnostic
        ''' Uses the neighbour search function to find the residue 'behind' the aromatic residue. It does this by
            finding the closest Oxygen atom to the CA atom of the aromatic. This should yield the residues right next to,
            the aromatic and the residue next to the residue behind the aromatic. it then returns the residue behind the
            aromatic'''
        centre = residue['CA'].get_coord()
        atom_list = Selection.unfold_entities(self.residue_list,  'A')  # Breaks each residue in the list into their corresponding atoms and coordinates
        ns = NeighborSearch(atom_list)  # initialises NeighborSearch function
        neighbours = ns.search(centre, 4.0) #3.5)
        neighbouring_atoms = Selection.unfold_entities(neighbours,'A')  # turns neighbouring atoms into list of corresponding atoms
        neighbouring_residues = []
        for atom in neighbouring_atoms:
            if atom.get_name() == 'O': #searches for what should be the closest residues to the aromatic by find the O atom
                neighbour_res = atom.get_parent()
                neighbouring_residues.append(neighbour_res)
        return_list = []
        for res in neighbouring_residues: #checks the residues aren't right next to the aromatic, and are an amino acid.
            if abs(residue.get_full_id()[3][1] - res.get_full_id()[3][1]) > 2:
                fullId = res.get_full_id()[3][1]
                nextId = fullId+1 #closest residue tends to be the one next to the res directly behind the aromatic
                nextID_Res = self.ps.get_residue_from_resseq(nextId)
                if nextID_Res != None and nextID_Res.get_resname() in self.AminoAcidLetters:
                    return_list.append(nextID_Res)
                else:
                    return_list.append(None)
        return return_list