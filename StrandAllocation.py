import os
from pathlib import Path

import numpy as np
from Bio.PDB import DSSP
from Bio.PDB import Selection
from sys import platform


FIXME
class StrandAllocation(object):

    def __init__(self, structure_id, structure, residue_list):
        self.residue_list = residue_list
        self.reference_key = None
        directory = os.path.dirname(__file__)
        model = Selection.unfold_entities(structure, 'M')[0]
        # pdb_name = os.path.join(directory, 'ordered_PDB', "pdb" + structure_id + ".ent")
        pdb_name = str(Path('./ordered_PDB/pdb{}.ent'.format(structure_id)))
        if platform == "linux" or platform == "linux2":
        # linux
            # print("Dected Linux OS using repository DSSP")
            self.dssp = DSSP(model, pdb_name, dssp='mkdssp', file_type='PDB')
        elif platform == "win32":
        # Windows...
            # print("Detected windows OS using binary DSSP file")
            # chains = Selection.unfold_entities(structure, 'C')
            # dssp_path = os.path.join(directory, 'dssp-3.0.0-win32.exe')
            dssp_path = str(Path('./dssp-3.0.0-win32.exe'))
            self.dssp = DSSP(model, pdb_name, dssp=dssp_path, file_type='PDB')
        self.keys = list(self.dssp.keys())

    def strand_calculator(self):
        ''' Goes through all the strands in a protein and merges some strands together
        based on the assumption that if the strands are pointing in the same direction and
        are close enough together, then perhaps they are been misclassified as two strands.The two
        strands and the residues in between are then added together to make one strand.'''
        first_chain = self.keys[0][0]
        total_strands = []
        current_strand = []
        interim_residues = []
        previous_residue = None
        for element in self.keys:
            if element[0] == first_chain:
                residue_stats = self.dssp[element]
                residue_type = residue_stats[2]
                if residue_type == 'E':
                    current_strand.append(element)
                if residue_type != 'E':
                    if previous_residue == 'E':
                        if len(interim_residues) > 4: #checks that the gap isn't too large, and if it is resets the search # may need to change where this goes, could be after vector comparison
                            total_strands.append(current_strand)
                            interim_residues = [element]
                            current_strand = []
                            previous_residue = residue_type
                            continue
                        if len(total_strands) >= 2: 
                            # Checks that the current strand and the previous strand are going in the same direction
                            vector1_residues = [self.DSSP_to_residue_object(total_strands[-1][-2][1][1]),
                                                self.DSSP_to_residue_object(total_strands[-1][-1][1][1])]  # 3rd last and last residue of previous strand
                            vector1 = self.vector_creator(vector1_residues)
                            vector2_residues = [self.DSSP_to_residue_object(current_strand[0][1][1]),
                                                self.DSSP_to_residue_object(current_strand[1][1][1])]  # 1st and 3rd res of current strand
                            vector2 = self.vector_creator(vector2_residues)
                            dotproduct = self.vector_comparator(vector1, vector2)
                            if dotproduct > 0:  # strands are going in the same direction
                                total_strands[-1]+=interim_residues
                                total_strands[-1]+=current_strand
                                current_strand = []
                                interim_residues = [element]
                            else:  # strands are pointing in opposite directions
                                total_strands.append(current_strand)
                                current_strand = []
                                interim_residues = [element]
                    else:
                        interim_residues.append(element)
                previous_residue = residue_type
            else:
                break
        for strand in total_strands:
            for x, residue in enumerate(strand):
                residue = self.DSSP_to_residue_object(residue[1][1])
                strand[x] = residue
        return total_strands

    def vector_creator(self, vector_residues):
        '''creates a vector generated from two residues' CA atoms.'''
        point_1 = vector_residues[0]['CA'].get_coord()
        point_2 = vector_residues[1]['CA'].get_coord()
        vector1 = np.subtract(point_2, point_1)
        return vector1

    def vector_comparator(self, vector1, vector2):
        '''calculates the dot product of two vectors so that you can
        determine if the vectors are going the same direction or not'''
        dotproduct = np.dot(vector1, vector2)
        return dotproduct

    def DSSP_to_residue_object(self, sequence_number):
        '''Turns a DSSP residue number into a corresponding structure residue object'''
        for residue in self.residue_list:
            if sequence_number == residue.get_full_id()[3][1]:
                return residue

    def residue_stats(self, residue_being_searched):
        '''Returns information about a residue calculated by DSSP.
            the information is a tuple in the form:
            (dssp index, amino acid, secondary structure, relative ASA, phi, psi,
            NH_O_1_relidx, NH_O_1_energy, O_NH_1_relidx, O_NH_1_energy,
            NH_O_2_relidx, NH_O_2_energy, O_NH_2_relidx, O_NH_2_energy)'''
        # DSSP data is accessed by a tuple (chain_id, res_id)
        # self.keys = list(self.dssp.keys())
        for element in self.keys:
            if str(element[1][1]) == str(residue_being_searched.get_full_id()[3][1]):
                reference_key = element
                res_stats = self.dssp[reference_key]
                # (dssp index, amino acid, secondary structure, relative ASA, phi, psi,
                # NH_O_1_relidx, NH_O_1_energy, O_NH_1_relidx, O_NH_1_energy,
                # NH_O_2_relidx, NH_O_2_energy, O_NH_2_relidx, O_NH_2_energy)
                return res_stats
        else:
            return False #TODO set a flag for the method that calls this, in case it return false

    def get_phipsi(self, residue):
        '''returns the phi and psi angles of a residue as calculated by DSSP'''
        # gets the phi and psi angles from the residue_stats variable.
        res_stat = self.residue_stats(residue)
        phi = res_stat[4]
        psi = res_stat[5]
        phipsi = [phi, psi]
        return phipsi