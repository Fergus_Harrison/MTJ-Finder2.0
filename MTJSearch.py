from itertools import chain

from Bio.PDB import NeighborSearch
from Bio.PDB import Selection

import get_torsion


class MTJSearch:

    def __init__(self, strand_list):
        strand_list_merged = chain.from_iterable(strand_list)
        self.strand_list = list(strand_list_merged)

    def aromatic_list(self):
        '''returns a list of all the aromatic residues found in the stands of the protein'''
        AROMATICS = ['TYR', 'PHE', 'TRP']
        aromatic_residues_list = []
        for residue in self.strand_list:
            if residue.get_resname() in AROMATICS:
                aromatic_residues_list.append(residue)
        return aromatic_residues_list

    def distance_measure(self, residue, glycine):
        '''Measures the distance between specific atoms (CA of glycine, CZ of Tyr and Phe, 
        and CE2 of TRP) of two residues. This used to measure the separation of the aromatic
        ring from the glycine in a mtj'''
        if residue.get_resname() == "TYR" or residue.get_resname() == "PHE":
            ca1 = residue['CZ']
        if residue.get_resname() == "TRP":
            ca1 = residue['CE2']
        ca2 = glycine['CA']
        distance = ca1 - ca2
        return distance

    def check_for_neighbours(self, aromatic_residue):
        '''searches for neighbouring strands of given "current strand"'''
        neighbour_list = []
        atom_list = Selection.unfold_entities(self.strand_list, 'A')  # Breaks each residue in the list into their corresponding atoms and coordinates
        ns = NeighborSearch(atom_list)  # initialises NeighborSearch function
        centre = aromatic_residue['CA'].get_coord() # Normally the centre atom is 'O', for  finding hydrogen bonding pairs
        found_in_radius = ns.search(centre, 4.5, level='A') # normally 3.5A, so it can find onyl hydrogen bonded pairings
        for atom in found_in_radius:
            # if atom.get_name() == 'N': #Normally uncommented, has been commented so the search can look for hydrogen bonded and hydrogen non-bonded interactions.
            residue_in_radius = atom.get_parent()
            if residue_in_radius.get_resname() == "GLY" and residue_in_radius not in neighbour_list:
                if abs(residue_in_radius.get_full_id()[3][1] - aromatic_residue.get_full_id()[3][1]) > 2:
                    neighbour_list.append(residue_in_radius)
        return neighbour_list # a list of all the neighbours to the aromatic that are GLY

    def check_for_gly(self, residue): # TODO Will only find glycines that have been designated as a strand, cannot find MTJ is DSSP mis-allocates the type of residue.
        '''Calculates the necessary torsion angles and proximity of the aromatic to glycine
           residues to determine if the residue forms an MTJ. Currently doesn't check for 
           torsion angles so that we can explore the idea that a MTJ isn't defined by it's 
           angles but more by it's steric hindrance and proximity of the aromatic ring to the
           glycine'''
        # self.phi_angle = phipsi[0]
        # self.psi_angle = phipsi[1]
        # self.torsion_angle = get_torsion.calculate_torsion(chi=[1, 2], res=residue) # calculates the chi1 and chi2 angles of the residue
        # if 45 < self.torsion_angle[0] < 75 and 80 < abs(self.torsion_angle[1]) < 100:  # checks for correct chi(1,2) parameters are fulfilled
        neighbouring_GLY = self.check_for_neighbours(residue)
        if len(neighbouring_GLY) > 0:
            return neighbouring_GLY
        else:
            return False

    def check_torsion(self, residue):
        '''A method that returns a boolean indicating whether the Chi angles fall within designated goal posts,
            and the corresponding chi1 and chi2 angles for that residue.
            It can return "BROKEN" if it cannot calculate the torsion angle for that residue'''
        self.torsion_angle = get_torsion.calculate_torsion(chi=[1, 2], res=residue) # calculates the chi1 and chi2 angles of the residue
        if self.torsion_angle == None:
            return "BROKEN", "BROKEN", "BROKEN"
        if 45 < self.torsion_angle[0] < 75 and 80 < abs(self.torsion_angle[1]) < 100:  # checks for correct chi(1,2) parameters are fulfilled
            return True, self.torsion_angle[0], self.torsion_angle[1]
        else:
            return False, self.torsion_angle[0], self.torsion_angle[1]

# MTJ search
        # DONE Search list of aromatic residues for residues that are adjacent to a glycine, regardless of aromatic chi angle (use Biopython neighbourSearch, might be a better search with the generated hydrogens).
        # DONE If neighbour_list > 1, check the chi angles of the aromatic residues
        # DONE Determine if the aromatic and glycine is facing inwards or outwards (create BarrelStats)
            # DONE If aromatic contains correct chi angles (45<chi1<75, 80<chi2<100) then it's considered a true MTJ.
                # DONE If true, then measure distance between aromtaic and glycine
            # If aromatic does not contain correct chi angles, then it's considered a false MTJ
        # All recordings of both true and false MTJs will contain: true or false MTJ, aromatic name, aromatic num, glycine num, distance b/w aromatic and glycine, Chi1/chi2.
        # DONE Check relative location of the MTJ to the centre of the protein barrel axis (as a function of the z-axis only).
            # DONE Create a new class called BarrelStats
            # DONE Create method that returns the z axis coord of the residue (used to determine the relative position in the barrel)
                # Positive number = it is closer to the outside, negative number = closer to the periplasm.
        # For both true and false MTJs find the inward facing residues to the left and right of the glycine, aromatic and left and right of the residue behind the aromatic.
        # Left and right residues can be determined using protein sequence (+-2 from residue of interest)
        # Rear residue can be determined using proximity of residue to the aromatic on a neighbouring strand (and is facing the same direction as the aromatic in/out).
        # To determine if the residue is facing inwards (dunbrack): compare the angle between the z-axis and the midpoint of its amino nitrogen and carbonyl carbon to its carbon alpha
        # If angle is <90 then it is facing inwards.