import pandas as pd
import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import seaborn as sns

#Combine the first and second true and false datafiles.
def collect_and_combine():
    false_first_df = pd.read_csv('false_output_file_newdef PDB_FH_revised.csv', delimiter=',',index_col=0).reset_index(drop=True)
    true_first_df = pd.read_csv('true_output_file_newdef PDB_FH_revised.csv', delimiter=',', index_col=0).reset_index(drop=True)
    false_first_df['true/false'] = 0
    true_first_df['true/false'] = 1
    combined_df = false_first_df.append(true_first_df, sort=False)

    # false_second_df = pd.read_csv('false_output_file_newdef second.csv', delimiter=',',index_col=0).reset_index(drop=True)
    # true_second_df = pd.read_csv('true_output_file_newdef second.csv', delimiter=',', index_col=0).reset_index(drop=True)
    # false_second_df['true/false'] = 0
    # true_second_df['true/false'] = 1
    # second_df = false_second_df.append(true_second_df, sort=False)

    # combined_df = first_df.append(second_df, sort=False)
    print(combined_df.columns)
    print(combined_df.head())
    print(combined_df.info())
    combined_df.to_csv('newdef_combined.csv')

def compare_old_and_new():
    # Compare the old and new def to find differences in detection
    newdef_df = pd.read_csv('newdef_combined.csv', delimiter=',',index_col=0).reset_index(drop=True)
    olddef_df = pd.read_csv('olddef_combined.csv', delimiter=',',index_col=0).reset_index(drop=True)
    newdef_df["version"] = 'new'
    olddef_df["version"] = 'old'
    print(newdef_df['native_chi1_rotamer'].describe())
    print(newdef_df['native_chi1_rotamer'].unique())
    print('trans count', newdef_df.loc[(newdef_df['native_chi1_rotamer']=='trans')&(newdef_df['true/false']==1), 'native_chi1_rotamer'].count())
    print('-gauche count', newdef_df.loc[(newdef_df['native_chi1_rotamer']=='-gauche')&(newdef_df['true/false']==1),'native_chi1_rotamer'].count())

    print(olddef_df['native_chi1_rotamer'].describe())
    print(olddef_df['native_chi1_rotamer'].unique())
    print('trans count', olddef_df.loc[(olddef_df['native_chi1_rotamer']=='trans')&(olddef_df['true/false']==1), 'native_chi1_rotamer'].count())
    print('-gauche count',olddef_df.loc[(olddef_df['native_chi1_rotamer']=='-gauche')&(olddef_df['true/false']==1), 'native_chi1_rotamer'].count())

    combined = newdef_df.append(olddef_df, sort=False)
        #Drop any duplicate columns, will highlight any changes in the algorithms findings.
    sns.countplot(x="native_chi1_rotamer", hue="version", data=combined.loc[combined['true/false']==1])
    plt.show()

    subset=['Protein_id', 'arom_residue_name', 'arom_residue_num', 'gly_residue_num', 'native_chi1_rotamer', 'true/false']
    combined.drop_duplicates(subset=subset,keep=False, inplace=True)
    combined.loc[combined['true/false']==1].drop_duplicates(keep=False, inplace=True)
    print(combined.loc[combined['true/false']==1])
    combined.to_csv('newold_comparison.csv')

# 'Protein_id', 'elucidation_method', 'Resolution', 'species',
    #    'protein_weight', 'protein_name', 'protein_family', 'quaternary',
    #    'contains_barrel', 'num_strands_in_barrel', 'arom_residue_name',
    #    'arom_residue_num', 'gly_residue_num', 'aromatic_phi', 'aromatic_psi',
    #    'glycine_phi', 'glycine_psi', 'chi1', 'chi2', 'native_chi1_rotamer',
    #    'vertical_placement', 'AN_residue', 'AC_residue', 'GN_residue',
    #    'GC_residue', 'rear_residue_name', 'rear_residue_num', 'RN_residue',
    #    'RC_residue', '+gauchetotal', '+gauchescore', '-gauchetotal',
    #    '-gauchescore', 'transtotal', 'transscore', 'lowest_rotamer_energy',
    #    'Distance b/w residues', 'true/false'

#explore the data in the newdef 
def explore():
    # Load csv and one-hot encode the categorical data
    newdef_df = pd.read_csv('newdef_combined.csv', delimiter=',',index_col=0).reset_index(drop=True)
    cols_of_interest = ['Protein_id', 'arom_residue_name', 'aromatic_phi', 'aromatic_psi', 'glycine_phi', 
                        'glycine_psi', 'chi1', 'chi2', 'native_chi1_rotamer', 'vertical_placement', 
                        '+gauchescore', '-gauchescore', 'transscore', 'lowest_rotamer_energy', 'true/false',
                        'Distance b/w residues']
    AA_mapping = {'ALA':0, 'ALAD':1, 'ARG':2, 'ASN':3, 'ASP':4, 'CYS':5, 'GLN':6, 'GLU':7, 'GLY':8, 'HSD':9, 
                'HSE':10, 'HSP':11, 'ILE':12, 'LEU':13, 'LYS':14, 'MET':15, 'PHE':16, 
                'PRO':17, 'SER':18, 'THR':19, 'TRP':20, 'TYR':21, 'VAL':22}
    rot_mapping = {'+gauche':1.0, 'trans':2.0, '-gauche':3.0}
    rot_columns = ['native_chi1_rotamer', 'lowest_rotamer_energy']
    res_columns = ['arom_residue_name']
    newdef_df = newdef_df[cols_of_interest]
    newdef_df['lowest_rotamer_energy'] = newdef_df['lowest_rotamer_energy'].map(lambda x: x[:-5])
    newdef_df[res_columns] = newdef_df[res_columns].applymap(AA_mapping.get)
    newdef_df[rot_columns] = newdef_df[rot_columns].applymap(rot_mapping.get)

    # print('true mtjs native rotamer description\n', newdef_df.loc[newdef_df['true/false']==1, 'native_chi1_rotamer'].describe())
    print(newdef_df.loc[newdef_df['true/false']==1, 'native_chi1_rotamer'].unique())
    print(newdef_df.loc[(newdef_df['native_chi1_rotamer'] > 1) & (newdef_df['true/false']==1)].head())

# filter_df = newdef_df[['native_chi1_rotamer', 'lowest_rotamer_energy', 'true/false']]
# sns.lmplot( "native_chi1_rotamer", "lowest_rotamer_energy", data=filter_df, hue='true/false', fit_reg=False)
# plt.show()

    corr_true = newdef_df.loc[newdef_df['true/false']==1].drop('true/false', axis=1).corr()
    corr_false = newdef_df.loc[newdef_df['true/false']==0].drop('true/false', axis=1).corr()
    print(newdef_df['native_chi1_rotamer'].loc[newdef_df['true/false']==1].describe())
    print(newdef_df['lowest_rotamer_energy'].loc[newdef_df['true/false']==1].describe())

# # print(newdef_df.info())
    ax = sns.heatmap(corr_true, xticklabels=corr_true.columns, yticklabels=corr_true.columns, annot=True)
    ax.set_title('true_mtjs')
    ax.figure.subplots_adjust(bottom = 0.3)
    plt.show()
    ax = sns.heatmap(corr_false, xticklabels=corr_false.columns, yticklabels=corr_false.columns, annot=True)
    ax.set_title('false_mtjs')
    ax.figure.subplots_adjust(bottom = 0.3)
    plt.show()
explore()