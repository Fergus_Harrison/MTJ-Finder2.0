import Bio.PDB
import numpy as np
from Bio.PDB import NeighborSearch, Selection, Vector
import matplotlib.pyplot as plt

class BarrelManipulation:
    def __init__(self,structure, residue_list, strand_list):
        self.structure = structure
        self.residue_list = residue_list
        self.atom_list = Selection.unfold_entities(self.residue_list, 'A')
        self.strand_list = strand_list.copy()

    def barrel_determination(self):
        """goes through strand list to see if protein contains a barrel"""
        self.previous_strands =[]
        self.ns = NeighborSearch(self.atom_list)
        self.current_strand = self.strand_list[0]
        self.barrel_search_complete = False
        while self.barrel_search_complete == False:
            self.check_strand_for_neighbour()
        return self.previous_strands

    def check_strand_for_neighbour(self):
        """searches for neighbouring strands of given 'current strand'"""
        for residue in self.current_strand:
            centre = residue['O'].get_coord()
            found_in_radius = self.ns.search(centre, 3.5, level='A')
            for atom in found_in_radius:
                if atom.get_name() == 'N':
                    residue_in_radius = atom.get_parent()
                    for strand in self.strand_list:
                        if residue_in_radius in strand and strand != self.current_strand \
                                            and strand not in self.previous_strands[-3:]:
                            if strand in self.previous_strands:
                                self.barrel_search_complete = True
                                self.previous_strands.append(self.current_strand)
                                return
                            else:
                                self.previous_strands.append(self.current_strand)
                                self.current_strand = strand
                                return
                        else:  # residue does not belong in any strand
                            continue

        self.strand_list.remove(self.current_strand)

        if len(self.previous_strands) > 0:  # checks if it can backdate to the previous strand
            self.current_strand = self.previous_strands.pop(-1)
        elif len(self.strand_list) > 0: #starts searching from on of the remaining strands
            self.current_strand = self.strand_list[0]
        else: #no barrel can be found
            self.barrel_search_complete = True
            return
        return

    def centroid_determination(self, barrel_strands):
        '''determines the center point of the top and bottom ellipse 
        created by the end residues of each strand in a barrel'''
        # find points of each ellipse
        ellipse1_residues = []
        ellipse2_residues = []
        ellipse1_coords = []
        ellipse2_coords = []
        for x, strand in enumerate(barrel_strands):
            if x % 2 == 0:
                ellipse1_residues.append(strand[-1])
                ellipse2_residues.append(strand[0])
            else:
                ellipse1_residues.append(strand[0])
                ellipse2_residues.append(strand[-1])

        for residue in ellipse1_residues:
            ellipse1_coords.append(residue['CA'].get_coord())
        for residue in ellipse2_residues:
            ellipse2_coords.append(residue['CA'].get_coord())
        ellipse1_coords_array = np.array(ellipse1_coords)
        ellipse2_coords_array = np.array(ellipse2_coords)
        self.first_ellipse_center = ellipse1_coords_array.sum(axis=0)/ellipse1_coords_array.size #sum of all the coordinates, divided by the number of coordinates
        self.second_ellipse_center = ellipse2_coords_array.sum(axis=0)/ellipse2_coords_array.size #sum of all the coordinates, divided by the number of coordinates
        self.barrel_axis = np.subtract(self.first_ellipse_center, self.second_ellipse_center)
        self.ellipse_centers = (self.first_ellipse_center, self.second_ellipse_center)
        return self.ellipse_centers

        ### CODE USED TO CHECK THAT THE ELLIPSE CENTRES WERE BEING ROTATED CORRECTLY
        # Xs = np.append(ellipse1_coords_array[:,0],ellipse2_coords_array[:,0])
        # Ys = np.append(ellipse1_coords_array[:,1],ellipse2_coords_array[:,1])
        # Zs = np.append(ellipse1_coords_array[:,2],ellipse2_coords_array[:,2])
        # self.visualise(Xs, Ys, Zs)

    def shortest_distance(self, vector_pointa, vector_pointb, point):
        '''finds the shortest distance between a point and the axis of barrel'''
        #could also use PDB.Vector.Vector_to_axis() method
        a = vector_pointa
        b = vector_pointb
        p = point
        n = self.normalise_vector(np.subtract(b,a)) # creates unit vector between a and b

        ap = np.subtract(p,a)
        t = ap.dot(n)
        x = np.add(a, t*n) #x is the closest point to P on the line
        px = np.subtract(p,x) # the vector between the point and the closest point on the line
        mag_px = np.sqrt(px[0]**2+px[1]**2+px[2]**2)
        return mag_px

    def check_correct_orientation(self):
        '''checks that the rotation of the barrel is rotated correctly so that the N terminal
        is pointing 'down' so that it matches how the protein would actually be rotated if the inside
        of the cell is 'down'.
        Should only be called once the barrel has been rotated so that the barrel axis 
        is aligned with the Z-axis.
        Works by Checking the Z coord of the CA atom of the N-Terminal residue and the CA atom of the last residue in the first strand of the barrel.
        If the N-terminal is higher than the last residue of the first strand: returns False
        else: returns True'''
        # if the N terminal has a larger z value than the end of the first strand, the protein is rotated upside down.
        # flip all the protein coordinates again to orient barrel correctly using rotate_whole_structure()
        print(self.residue_list[0])
        try:  # 'CA' and 'C' are tried to account for terminal carbon allocations in different PDBs
            N_terminal = self.residue_list[0]["CA"].get_coord() # find the z coord of the N terminal
        except KeyError:
            N_terminal = self.residue_list[0]["C"].get_coord()
        try:
            barrel_end = self.strand_list[0][-1]["CA"].get_coord() # find the z coord of the other side of barrel
        except KeyError:
            barrel_end = self.strand_list[0][-1]["C"].get_coord()
        if N_terminal[2] > barrel_end[2]:
            return False #indicates that protein is upside down
        else:
            return True

    def magnitude(self, point):
        '''returns the magnitude of the given vector'''
        mag = np.sqrt(point[0]**2+point[1]**2+point[2]**2)
        return mag

    def normalise_vector(self, point):
        '''takes a vector and returns the unit vector with magnitude = 1'''
        magnitude = self.magnitude(point)
        unit_vector = np.array([point[0]/magnitude, point[1]/magnitude, point[2]/magnitude])
        return unit_vector

    def visualise(self, Xs, Ys, Zs):
        '''local method used to debug the residue selection of the barrel ellipses and their rotation'''
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.plot(Xs, Ys, Zs, c='c')
        plt.show()
        return

    def transform(self, rotation, translation, structure):
        '''a method that translates and rotates a given structure atom by atom at the same time.
            makes rotate_structure and translate_structure redundant'''
        for atom in structure.get_atoms():
            new_coord = atom.get_vector() - translation 
            atom.set_coord(new_coord.get_array())
        for atom in structure.get_atoms():
            new_coord = atom.get_vector().left_multiply(rotation) 
            atom.set_coord(new_coord.get_array())
        return structure

    def translate_structure(self, structure):
        '''translates the structure atom by atom so that the midpoint of the barrel is at the origin of
        the cartesian plane'''
        # Find the midpoint of the central axis
        midpoint = self.midpoint()
        # subtract the midpoint from every point in the structure. should have it centred at the origin
        for atom in structure.get_atoms():
            atom_current_coord = atom.get_coord()
            atom_new_coord = np.subtract(atom_current_coord, midpoint)
            atom.set_coord(atom_new_coord)
        return structure

    def rotate_structure(self, rotation_matrix, structure):
        '''manually rotates the structure according to a rotation matrix atom by atom. 
        Seems to do a more stable job than the transform function provided by Biopython'''
        for atom in structure.get_atoms():
            atom_current_coord = atom.get_coord()
            atom_new_coord = np.dot(atom_current_coord, rotation_matrix)
            atom.set_coord(atom_new_coord)
        return structure

    def get_barrel_rotation(self, moving_axis, final_axis):
        '''returns the matrix necessary to rotate the structure to a given axis,
        uses Biopython's rotmat function'''
        # z_axis = Vector([0,0,1])
        # unit_barrel_axis = Vector(np.reshape(self.normalise_vector(self.barrel_axis),3))
        rotation_matrix = Bio.PDB.rotmat(moving_axis, final_axis)
        # print("moving_axis:", moving_axis)
        # print("final_axis:", final_axis)
        # print("rotation_matrix:", rotation_matrix)
        return rotation_matrix

    def midpoint(self):
        midpoint = np.add(self.ellipse_centers[0],self.ellipse_centers[1])/2
        # half_length_of_axis = np.multiply(0.5,self.magnitude(central_axis))
        midpoint = np.reshape(midpoint,3)
        return midpoint