import csv
import os
from pathlib import Path

import pandas as pd


class SCOPe_parser(object):

    def __init__(self, structure_code):
        self.pdbcode = structure_code
        script_dir = os.path.dirname(__file__)  # <-- absolute dir the script is in
        # cla_path = "SCOPe/dir.cla.scope.2.07-stable.txt"
        # des_path ="SCOPe/dir.des.scope.2.07-stable.txt"
        cla_abs_file_path = Path("./SCOPe/dir.cla.scope.2.07-stable.txt")
        des_abs_file_path = Path("./SCOPe/dir.des.scope.2.07-stable.txt")
        # cla_abs_file_path = os.path.join(script_dir, cla_path)
        # des_abs_file_path = os.path.join(script_dir, des_path)
        self.classification_file = pd.read_csv(cla_abs_file_path, comment='#', header=None, delim_whitespace=True, names=['sid','PDB_ID','description','sccs','sunid','ancestor_ids'])
        self.description_file = pd.read_csv(des_abs_file_path, comment='#',sep='\t', header=None, names=['sunid', 'level','sccs','sid','description'])
        try:
            self.sunid = self.classification_file.loc[self.classification_file['PDB_ID']==self.pdbcode, 'sunid'].iloc[0]
        except (KeyError, IndexError):
            # didn't find anything
            print("couldn't find a record in SCOPe")
            self.sunid = None

    @property
    def name(self):
        '''returns the name of the protein by looking at the Scope 
        database parseable files for the PDB id'''
        if self.sunid == None:
            return None
        else:
            ancestor_nodes = self.dict_from_series('=',self.classification_file.loc[self.classification_file['PDB_ID']==self.pdbcode, 'ancestor_ids'].iloc[0])
            level_sunid = ancestor_nodes['dm']
            protein_name = self.description_file.loc[self.description_file['sunid']==level_sunid,'description'].iloc[0]
            return protein_name

    @property
    def prot_class(self):
        '''Returns the Class of the protein according to the Scope Database'''
        if self.sunid == None:
            return None
        else:
            ancestor_nodes = self.dict_from_series('=',self.classification_file.loc[self.classification_file['PDB_ID']==self.pdbcode, 'ancestor_ids'].iloc[0])
            level_sunid = ancestor_nodes['cl']
            protein_class = self.description_file.loc[self.description_file['sunid']==level_sunid,'description'].iloc[0]
            return protein_class

    @property
    def fold(self):
        '''returns the fold type of the protein according to the Scope Database'''
        if self.sunid == None:
            return None
        else:
            ancestor_nodes = self.dict_from_series('=',self.classification_file.loc[self.classification_file['PDB_ID']==self.pdbcode, 'ancestor_ids'].iloc[0])
            level_sunid = ancestor_nodes['cf']
            protein_fold = self.description_file.loc[self.description_file['sunid']==level_sunid,'description'].iloc[0]
            return protein_fold

    @property
    def Superfamily(self):
        '''Returns the superfamily of the protein according to the Scope Database'''
        if self.sunid == None:
            return None
        else:
            ancestor_nodes = self.dict_from_series('=',self.classification_file.loc[self.classification_file['PDB_ID']==self.pdbcode, 'ancestor_ids'].iloc[0])
            level_sunid = ancestor_nodes['sf']
            protein_superfamily = self.description_file.loc[self.description_file['sunid']==level_sunid,'description'].iloc[0]
            return protein_superfamily

    @property
    def family(self):
        '''Returns the family of the protein according to the Scope Database'''
        if self.sunid == None:
            return None
        else:
            ancestor_nodes = self.dict_from_series('=',self.classification_file.loc[self.classification_file['PDB_ID']==self.pdbcode, 'ancestor_ids'].iloc[0])
            level_sunid = ancestor_nodes['fa']
            protein_family = self.description_file.loc[self.description_file['sunid']==level_sunid,'description'].iloc[0]
            return protein_family

    @property
    def species(self):
        '''Returns the species of the protein according to the Scope Database'''
        if self.sunid == None:
            return None
        else:
            ancestor_nodes = self.dict_from_series('=',self.classification_file.loc[self.classification_file['PDB_ID']==self.pdbcode, 'ancestor_ids'].iloc[0])
            level_sunid = ancestor_nodes['sp']
            protein_species = self.description_file.loc[self.description_file['sunid']==level_sunid,'description'].iloc[0]
            return protein_species

    @property
    def getSunid(self):
        '''Gets the sunid of the protein for the Scope database, the sunid is kind of like
        the master reference for the protein'''
        return self.sunid

    def dict_from_series(self, delimiter, givenlist):
        '''creates a dictionary object from a comma separated list.
        e.g. x1=y1,x2=y2,x3=y3, etc...'''
        final_dict = {}
        givenlist = givenlist.split(',')
        for item in givenlist:
            elements = item.split(delimiter)
            try:
                final_dict[elements[0]] = int(elements[1])
            except IndexError:
                continue
        return final_dict
