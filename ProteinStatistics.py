import numpy as np
from Bio.PDB import NeighborSearch
from Bio.PDB import Selection
from Bio.PDB import Vector
from Bio.SeqUtils.ProtParam import ProteinAnalysis


class ProteinStatistics:

    def __init__(self, structure, residue_list):
        self.structure = structure
        self.residue_list = residue_list
        self.AminoAcidLetters = {'ARG':'R', 'HIS':'H', 'LYS':'K', 'ASP':'D',
                                 'GLU':'E', 'SER':'S', 'THR':'T', 'ASN':'N',
                                 'GLN':'Q', 'CYS':'C', 'SEC':'U', 'GLY':'G',
                                 'PRO':'P', 'ALA':'A', 'ILE':'I', 'LEU':'L',
                                 'MET':'M', 'PHE':'F', 'TRP':'W', 'TYR':'Y',
                                 'VAL':'V'}

    def directional_vector(self, residue): #protein agnostic
        '''An amino acid’s directional vector was defined as the direction from the midpoint of
        its amino nitrogen and its carbonyl carbon to its carbon alpha'''
        nitrogen = residue["N"]
        carbyl = residue["CA"]
        midpoint = self.midpoint(nitrogen, carbyl)
        alpha_carbon = residue["C"].get_coord()
        dir_vec = np.subtract(alpha_carbon, midpoint)
        return Vector(dir_vec)

    def midpoint(self, nitrogen, carbon): #protein agmostic
        '''returns the midpoint between two atoms'''
        nit = nitrogen.get_coord()
        carb = carbon.get_coord()
        x = (nit[0]+carb[0])/2
        y = (nit[1]+carb[1])/2
        z = (nit[2]+carb[2])/2
        midpoint = np.array([x,y,z])
        return midpoint

    def res_adj_AAs(self, residue): #protein agnostic
        '''Returns the amino acids that are next to a given amino acid in the N and C terminal direction.
            The adjacent amino acids are always the second residue away from the given residue 
            because they will be facing the same direction in a beta-sheet'''
        res_id = residue.get_full_id()[3][1]
        previous_aa_id = res_id -2
        previous_aa = self.get_residue_from_resseq(previous_aa_id)
        next_aa_id = res_id + 2
        next_aa = self.get_residue_from_resseq(next_aa_id)
        if previous_aa != None:
            previous_aa_name = previous_aa.get_resname()
        else:
            previous_aa_name = "None"
        if next_aa != None:
            next_aa_name = next_aa.get_resname()
        else:
            next_aa_name = "None"
        return previous_aa_name, next_aa_name

    def get_residue_from_resseq(self, resseq): #protein agnostic
        '''Takes the residue number (as according to the PDB file) 
        and finds the corresponding residue object and returns it'''
        for residue in self.residue_list:
            if residue.get_full_id()[3][1] == resseq:
                return residue

    def protein_weight(self): #protein agnostic
        '''Returns the weight of the protein in kDa'''
        protein_sequence = []
        for residue in self.residue_list:
            name = residue.get_resname()
            if name not in self.AminoAcidLetters:
                continue
            letter = self.AminoAcidLetters[name]
            protein_sequence.append(letter)
        protein_sequence = ''.join(protein_sequence)
        pa = ProteinAnalysis(protein_sequence) #gives a string of amino acid letter abbreviations to a method that will sum the mass of each amino acid 
        weight = pa.molecular_weight()
        return weight

    def magnitude(self, point): # protein agnostic
        '''Returns the magnitude of a vector'''
        mag = np.sqrt(sum(point**2))
        return mag

    def normalise_vector(self, point): #protein agnostic
        '''takes a vector and returns the unit vector with magnitude = 1'''
        point = point.get_array()
        mag = self.magnitude(point)
        unit_vector = np.array(point/mag)
        return unit_vector

    def angle(self, first_vector, second_vector): #protein agnostic
        '''Return angle between two vectors.'''
        n1 = self.normalise_vector(first_vector)
        n2 = self.normalise_vector(second_vector)
        return np.arccos(np.clip(np.dot(n1, n2), -1.0, 1.0))

def magnitude(point): # protein agnostic
    '''Returns the magnitude of a vector'''
    mag = np.sqrt(sum(point**2))
    return mag

def normalise_vector(point): #protein agnostic
    '''takes a vector and returns the unit vector with magnitude = 1'''
    mag = magnitude(point)
    unit_vector = np.array(point/mag)
    return unit_vector

def angle(first_vector, second_vector): #protein agnostic
    '''Return angle between two vectors.'''
    n1 = normalise_vector(first_vector)
    n2 = normalise_vector(second_vector)
    return np.arccos(np.clip(np.dot(n1, n2), -1.0, 1.0))
